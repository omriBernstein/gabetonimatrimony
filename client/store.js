import {createStore, applyMiddleware} from 'redux';

import rootReducer from './root.reducer';
import iterableMiddleware from './utils/iterable-middleware';
import promiseMiddleware from './utils/promise-middleware';

const store = createStore(
  rootReducer,
  applyMiddleware(
    iterableMiddleware,
    promiseMiddleware,
    // () => (next) => (action) => {
    //   console.log('action', action);
    //   return next(action);
    // }
  )
);

export default store;
