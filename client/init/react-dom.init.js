import ReactDOM from 'react-dom';
import React from 'react';

import Root from '../components/Root.component';

ReactDOM.render(
  <Root />,
  document.getElementById('root')
);
