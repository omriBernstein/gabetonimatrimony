import store from '../store';
import {focused, blurred} from '../ducks/window-has-focus.duck';

window.addEventListener('focus', () => {
  store.dispatch(focused());
});

window.addEventListener('blur', () => {
  store.dispatch(blurred());
});
