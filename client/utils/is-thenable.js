const isThenable = (thing) => {
  return thing && typeof thing.then === 'function';
};

export default isThenable;
