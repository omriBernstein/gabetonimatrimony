function eachkv (obj, iterator) {
  Object.keys(obj).forEach(k => iterator(k, obj[k]));
}

export default eachkv;
