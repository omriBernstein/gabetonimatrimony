const isIterable = (thing) => {
  return thing && typeof thing[Symbol.iterator] === 'function';
};

export default isIterable;
