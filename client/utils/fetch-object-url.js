function fetchObjectUrl (url) {
  return new Promise(function (resolve, reject) {
    const request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'blob';
    request.onload = function () {
      const objUrl = URL.createObjectURL(this.response);
      resolve(objUrl);
    };
    request.onerror = reject;
    request.send();
  });
}

export default fetchObjectUrl;
