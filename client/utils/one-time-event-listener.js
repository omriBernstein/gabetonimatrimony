function oneTimeEventListener (element, event, listener) {
  function selfRemovingListener (...args) {
    listener.apply(this, args);
    element.removeEventListener(event, selfRemovingListener);
  }
  element.addEventListener(event, selfRemovingListener);
}

export default oneTimeEventListener;
