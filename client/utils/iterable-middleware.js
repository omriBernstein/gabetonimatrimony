import isIterable from './is-iterable';

const iterableMiddleware = ({dispatch}) => (next) => (action) => {
  if (isIterable(action)) {
    for (const subaction of action) {
      dispatch(subaction);
    }
    return action;
  } else {
    return next(action);
  }
};

export default iterableMiddleware;
