const once = (fn) => {
  let called = false;
  const output = {};
  return function (...args) {
    if (called) {
      if (output.type === 'return') {
        return output.result;
      } else if (output.type === 'throw') {
        throw output.result;
      }
    }
    called = true;
    try {
      output.result = fn.apply(this, args);
      output.type = 'return';
      return output.result;
    } catch (error) {
      output.type = 'throw';
      throw output.result;
    }
  };
};

export default once;
