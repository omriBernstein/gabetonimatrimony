import isThenable from './is-thenable';

const promiseMiddleware = ({dispatch}) => (next) => (action) => {
  if (isThenable(action)) {
    return action.then(dispatch);
  } else {
    return next(action);
  }
};

export default promiseMiddleware;
