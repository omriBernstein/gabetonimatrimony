const withinBounds = (value, min, max) => {
  return Math.min(Math.max(value, min), max);
};

export default withinBounds;
