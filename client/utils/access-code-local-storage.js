import axios from 'axios';

const accessCodeKey = 'gabetonimatrimony/accessCode';

export const storeAccessCode = (accessCode) => {
  axios.defaults.params = {accessCode};
  return localStorage.setItem(accessCodeKey, accessCode);
};

export const retrieveAccessCode = () => {
  const accessCode = localStorage.getItem(accessCodeKey);
  if (accessCode) {
    axios.defaults.params = {accessCode};
  } else {
    axios.defaults.params = {};
  }
  return accessCode;
};
