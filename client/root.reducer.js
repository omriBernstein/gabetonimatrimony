import {combineReducers} from 'redux';

import road from './ducks/road.duck';
import deltaActive from './ducks/delta-active.duck';
import me from './ducks/me.duck';
import passedGateway from './ducks/passed-gateway.duck';
import backgroundAudio from './ducks/background-audio.duck';
import windowHasFocus from './ducks/window-has-focus.duck';

const rootReducer = combineReducers({
  road,
  deltaActive,
  me,
  passedGateway,
  backgroundAudio,
  windowHasFocus
});

export default rootReducer;
