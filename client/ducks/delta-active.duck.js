import eachkv from '../utils/eachkv';

const initialState = {
  forwards: false,
  backwards: false
};

const prefix = 'gabetonimatrimony/deltaActive';

export const SET_FORWARDS = `${prefix}/SET_FORWARDS`;
export const setForwards = (payload) => ({
  type: SET_FORWARDS,
  payload
});

export const SET_BACKWARDS = `${prefix}/SET_BACKWARDS`;
export const setBackwards = (payload) => ({
  type: SET_BACKWARDS,
  payload
});

const transformers = {};

const applyChanges = (state, fields) => {
  const diff = {};
  eachkv(fields, (field, next) => {
    const previous = state[field];
    const transformer = transformers[field];
    const transformed = typeof transformer === 'function' ? transformer(next, previous, state) : next;
    diff[field] = transformed;
  });
  return Object.assign({}, state, diff);
};

const reducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_FORWARDS:
      return applyChanges(state, {
        forwards: payload
      });
    case SET_BACKWARDS:
      return applyChanges(state, {
        backwards: payload
      });
    default:
      return state;
  }
};

export default reducer;
