const initialState = false;

const prefix = 'gabetonimatrimony/passedGateway';

export const PASS_GATEWAY = `${prefix}/PASS_GATEWAY`;
export const passGateway = () => ({
  type: PASS_GATEWAY
});

const reducer = (state = initialState, {type}) => {
  switch (type) {
    case PASS_GATEWAY:
      return true;
    default:
      return state;
  }
};

export default reducer;
