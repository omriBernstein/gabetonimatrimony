import {play} from './background-audio.duck';
import {pitStops} from '../constants';
import eachkv from '../utils/eachkv';

const stopTimes = Array.from(pitStops(true).keys());
const maxStopTime = Math.max(...stopTimes);
const minStopTime = Math.min(...stopTimes);

const initialState = {
  duration: 0,
  prospectiveTime: minStopTime,
  currentTime: minStopTime,
  stuck: false,
  previousStop: -1,
  nextStop: 1,
  currentStop: 0,
  loading: true
};

const prefix = 'gabetonimatrimony/road';

export const SET_PROSPECTIVE_TIME = `${prefix}/SET_PROSPECTIVE_TIME`;
export const setProspectiveTime = (payload) => ({
  type: SET_PROSPECTIVE_TIME,
  payload
});

export const SET_CURRENT_TIME = `${prefix}/SET_CURRENT_TIME`;
export const setCurrentTime = (payload) => ({
  type: SET_CURRENT_TIME,
  payload
});

export const SET_STOP = `${prefix}/SET_STOP`;
export const setStop = (payload) => ({
  type: SET_STOP,
  payload
});

export const SET_DURATION = `${prefix}/SET_DURATION`;
export const setDuration = (payload) => ({
  type: SET_DURATION,
  payload
});

export const APPLY_DELTA = `${prefix}/APPLY_DELTA`;
export const applyDelta = (payload) => ({
  type: APPLY_DELTA,
  payload
});

export const UNSTICK = `${prefix}/UNSTICK`;
export const unstick = () => ({
  type: UNSTICK
});

export const ROAD_LOADED = `${prefix}/ROAD_LOADED`;
export const roadLoaded = () => ([{
  type: ROAD_LOADED
}, play()]);

const transformers = {
  prospectiveTime: (next, previous, {duration}) => {
    if (typeof next !== 'number') return previous;
    return Math.min(Math.max(minStopTime, next), duration, maxStopTime);
  }
};

const applyChanges = (state, fields) => {
  const diff = {};
  eachkv(fields, (field, next) => {
    const previous = state[field];
    const transformer = transformers[field];
    const transformed = typeof transformer === 'function' ? transformer(next, previous, state) : next;
    diff[field] = transformed;
  });
  return Object.assign({}, state, diff);
};

const stickTo = (state, currentStop) => {
  return applyChanges(state, {
    prospectiveTime: stopTimes[currentStop],
    stuck: true
  });
};

const stopAt = (state, currentStop) => {
  return applyChanges(state, {
    currentTime: stopTimes[currentStop],
    currentStop,
    previousStop: currentStop - 1,
    nextStop: currentStop + 1
  });
};

const unstopAt = (state, previousOrNext) => {
  return applyChanges(state, {
    currentStop: null,
    [previousOrNext]: state.currentStop
  });
};

const reducer = (state = initialState, {type, payload}) => {
  const {
    prospectiveTime,
    currentStop,
    previousStop,
    nextStop,
    stuck
  } = state;
  switch (type) {
    case SET_STOP:
      return applyChanges(state, {
        prospectiveTime: stopTimes[payload],
        stuck: false
      });
    case SET_CURRENT_TIME:
      const possibleStop = stopTimes.indexOf(payload);
      if (possibleStop !== -1) {
        return stopAt(state, possibleStop);
      }
      if (payload < stopTimes[currentStop]) {
        state = unstopAt(state, 'nextStop');
      }
      if (payload > stopTimes[currentStop]) {
        state = unstopAt(state, 'previousStop');
      }
      return applyChanges(state, {currentTime: payload});
    case SET_PROSPECTIVE_TIME:
      return applyChanges(state, {prospectiveTime: payload});
    case SET_DURATION:
      return applyChanges(state, {duration: payload});
    case UNSTICK:
      return applyChanges(state, {stuck: false});
    case APPLY_DELTA:
      if (stuck) return state;
      const upcomingTime = prospectiveTime + payload;
      if (upcomingTime < stopTimes[previousStop]) {
        return stickTo(state, previousStop);
      }
      if (upcomingTime > stopTimes[nextStop]) {
        return stickTo(state, nextStop);
      }
      return applyChanges(state, {prospectiveTime: upcomingTime});
    case ROAD_LOADED:
      return applyChanges(state, {loading: false});
    default:
      return state;
  }
};

export default reducer;
