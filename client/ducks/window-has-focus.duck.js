const initialState = document.hasFocus();

const prefix = 'gabetonimatrimony/windowHasFocus';

export const FOCUSED = `${prefix}/FOCUSED`;
export const focused = () => ({
  type: FOCUSED
});

export const BLURRED = `${prefix}/BLURRED`;
export const blurred = () => ({
  type: BLURRED
});

const reducer = (state = initialState, {type}) => {
  switch (type) {
    case FOCUSED:
      return true;
    case BLURRED:
      return false;
    default:
      return state;
  }
};

export default reducer;
