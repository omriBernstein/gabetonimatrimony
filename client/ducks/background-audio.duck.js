const initialState = {
  playing: false
};

const prefix = 'gabetonimatrimony/backgroundAudio';

export const PLAY = `${prefix}/PLAY`;
export const play = () => ({
  type: PLAY
});

export const PAUSE = `${prefix}/PAUSE`;
export const pause = () => ({
  type: PAUSE
});

const reducer = (state = initialState, {type}) => {
  switch (type) {
    case PLAY:
      return {...state, playing: true};
    case PAUSE:
      return {...state, playing: false};
    default:
      return state;
  }
};

export default reducer;
