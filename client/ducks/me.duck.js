import axios from 'axios';

import {storeAccessCode, retrieveAccessCode} from '../utils/access-code-local-storage';

const initialState = {};

const prefix = 'gabetonimatrimony/me';

export const SET_ME = `${prefix}/SET_ME`;
export const setMe = (me) => ({
  type: SET_ME,
  payload: me
});
export const login = (accessCode) => {
  return axios.put('/api/me', {}, {params: {accessCode}})
  .then((response) => {
    storeAccessCode(accessCode);
    return setMe(response.data);
  });
};

export const unsetMe = () => ({
  type: SET_ME,
  payload: initialState
});
export const logout = () => {
  return axios.delete('/api/me')
  .then(() => {
    storeAccessCode(null);
    return unsetMe();
  });
};

export const fetchMe = () => {
  const accessCode = retrieveAccessCode();
  if (!accessCode) return Promise.resolve(unsetMe());
  return axios.get('/api/me', {params: {accessCode}})
  .then(response => setMe(response.data))
  .catch(() => {
    storeAccessCode(null);
    return unsetMe();
  });
};

export const updateMe = (id, updates) => {
  return axios.put(`/api/users/${id}`, updates)
  .then(response => setMe(response.data));
};

const reducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_ME:
      return payload;
    default:
      return state;
  }
};

export default reducer;
