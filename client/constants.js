import browser from 'detect-browser';

import Welcome from './components/Welcome.component';
import OurStory from './components/OurStory.component';
import EngagementStory from './components/EngagementStory.component';
import WeddingParty from './components/WeddingParty.component';
import EventDetails from './components/EventDetails.component';
import Attire from './components/Attire.component';
import GiftRegistry from './components/GiftRegistry.component';
import RSVP from './components/RSVP.component';
import ThanksForVisiting from './components/ThanksForVisiting.component';

export const excludesRegistry = window.location.pathname === '/x';

export const pitStops = (isInvited) => new Map([
  [0.1, Welcome],
  [11.9, OurStory],
  [21.8, EngagementStory],
  [32.7, WeddingParty],
  [43.8, excludesRegistry ? ThanksForVisiting : EventDetails]
].concat(excludesRegistry ? [] : [
  [58.9, Attire],
  [75.1, GiftRegistry],
  [85.5, isInvited ? RSVP : ThanksForVisiting]
]));

export const pitStopUrls = (isInvited) => ([
  '/welcome',
  '/our-story',
  '/the-proposal',
  '/wedding-party',
  excludesRegistry ? '/thanks-for-visiting' : '/event-details'
].concat(excludesRegistry ? [] : [
  '/attire',
  '/gift-registry',
  isInvited ? '/rsvp' : '/thanks-for-visiting'
]));

export const googleMapStyle = [{
  featureType: 'landscape',
  stylers: [{
    hue: '#FFA800'
  }, {
    saturation: 0
  }, {
    lightness: 0
  }, {
    gamma: 1
  }]
}, {
  featureType: 'road.highway',
  stylers: [{
    hue: '#53FF00'
  }, {
    saturation: -73
  }, {
    lightness: 40
  }, {
    gamma: 1
  }]
}, {
  featureType: 'road.arterial',
  stylers: [{
    hue: '#FBFF00'
  }, {
    saturation: 0
  }, {
    lightness: 0
  }, {
    gamma: 1
  }]
}, {
  featureType: 'road.local',
  stylers: [{
    hue: '#00FFFD'
  }, {
    saturation: 0
  }, {
    lightness: 30
  }, {
    gamma: 1
  }]
}, {
  featureType: 'water',
  stylers: [{
    hue: '#00BFFF'
  }, {
    saturation: 6
  }, {
    lightness: 8
  }, {
    gamma: 1
  }]
}, {
  featureType: 'poi',
  stylers: [{
    hue: '#679714'
  }, {
    saturation: 33.4
  }, {
    lightness: -25.4
  }, {
    gamma: 1
  }]
}];

const scrollNavigationBrowsers = new Set([
  'safari',
  'ios',
  'edge',
  'chrome'
]);

export const scrollNavigatingEnabled = scrollNavigationBrowsers.has(browser.name);

const throttleTimes = {
  safari: 50,
  ios: 50,
  edge: 150,
  chrome: 150,
  default: 150
};

export const throttleTime = throttleTimes[browser.name] || throttleTimes.default;

const debounceTimes = {
  safari: 250,
  ios: 250,
  edge: 250,
  chrome: 250,
  default: 250
};

export const debounceTime = debounceTimes[browser.name] || debounceTimes.default;

const deltaEffects = {
  safari: 0.5,
  ios: 0.2,
  edge: 1,
  chrome: 0.3,
  default: 0.3
};

export const deltaEffect = deltaEffects[browser.name] || deltaEffects.default;
