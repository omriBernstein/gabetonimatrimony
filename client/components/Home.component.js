import React from 'react';

import RoadContainer from '../containers/Road.container';
import PitStopContainer from '../containers/PitStop.container';
import NavigateContainer from '../containers/Navigate.container';
import ScrollCaptureContainer from '../containers/ScrollCapture.container';
import BackgroundAudioContainer from '../containers/BackgroundAudio.container';

const topLevelStyle = {
  width: '100vw',
  height: '100vh',
  overflow: 'hidden'
};

const Home  = () => (
  <ScrollCaptureContainer>
    <div className='main-font' style={topLevelStyle}>
      <BackgroundAudioContainer />
      <PitStopContainer />
      <RoadContainer />
      <NavigateContainer />
    </div>
  </ScrollCaptureContainer>
);

export default Home;
