import React from 'react';
import {Header} from 'semantic-ui-react';

const PitStopHeader = ({content, children}) => (
  <Header as='h1' textAlign='center' style={{marginTop: '1.5rem'}}>
    <span style={{fontSize: '1.8em', lineHeight: '1.1em'}}>{content || children}</span>
  </Header>
);

export default PitStopHeader;
