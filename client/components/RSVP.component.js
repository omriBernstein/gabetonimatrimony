import React, {Component} from 'react';
import {Container} from 'semantic-ui-react';
import Transition from 'react-transition-group/Transition';

import Video from './Video.component';
import RSVPFormContainer from '../containers/RSVPForm.container';
import PitStopHeader from './PitStopHeader.component';

const swirlDuration = 2000;

const plungeVideoDefaultStyle = {
  transformOrigin: '0 0',
  position: 'fixed',
  zIndex: -1
};

const plungeVideoStyles = {
  'pre-swirling': {
    transition: 'none',
    transform: 'scale(0) rotate(-270deg) translate(-50%, -50%)'
  },
  swirling: {
    transition: `transform ${swirlDuration}ms ease-in-out`,
    transform: 'scale(1) rotate(0deg) translate(-50%, -50%)'
  },
  playing: {
    transition: 'none',
    transform: 'scale(1) rotate(0deg) translate(-50%, -50%)'
  },
  textFreezeFrame: {
    transition: 'none',
    transform: 'scale(1) rotate(0deg) translate(-50%, -50%)'
  },
  done: {
    transition: 'none',
    transform: 'scale(1) rotate(0deg) translate(-50%, -50%)'
  }
};

class RSVP extends Component {
  constructor () {
    super();
    this.state = {
      stage: 'pre-swirling'
    };
  }
  componentWillReceiveProps (nextProps) {
    if (this.props.active && !nextProps.active) {
      this.setState({
        stage: 'pre-swirling'
      });
      if (this._node) {
        this._node.pause();
      }
      if (this._willPlayTimer) {
        clearTimeout(this._willPlayTimer);
      }
    }
    if (!this.props.active && nextProps.active) {
      setTimeout(() => {
        this.maybeStartSwirling();
      }, 1);
    }
  }
  componentDidMount () {
    this.didMount = true;
    this.maybeStartSwirling();
  }
  maybeStartSwirling () {
    const {active} = this.props;
    const swirling = this.state.stage === 'swirling';
    if (this.didMount && this.canPlayThrough && active && !swirling) {
      this.setState({
        stage: 'swirling'
      });
    }
  }
  videoRef = (node) => {
    if (node) this.videoMount(node);
    else this.videoUnmount();
  }
  videoMount (node) {
    this._node = node;
    this._node.addEventListener('canplaythrough', this.onCanPlayThrough);
    this._node.addEventListener('ended', this.onEnded);
  }
  videoUnmount () {
    this._node.removeEventListener('canplaythrough', this.onCanPlayThrough);
    this._node.removeEventListener('ended', this.onEnded);
  }
  onCanPlayThrough = () => {
    this.canPlayThrough = true;
    this.maybeStartSwirling();
  }
  onEnded = () => {
    this.setState({
      stage: 'textFreezeFrame'
    }, () => {
      setTimeout(() => {
        this.setState({
          stage: 'done'
        });
      }, 1000);
    });
  }
  render () {
    const {stage} = this.state;
    const videoNodeProps = {};
    if (stage === 'pre-swirling') {
      videoNodeProps.currentTime = 0;
    }
    return (
      <Transition
        in={stage === 'swirling'}
        timeout={swirlDuration}
        onEntering={() => {
          this._willPlayTimer = setTimeout(() => {
            this._node.play();
          }, swirlDuration - 800);
        }}
        onEntered={() => {
          this.setState({
            stage: 'playing'
          });
        }}>
        <div>
          <div style={{
            backgroundColor: 'black',
            height: '100vh',
            width: '100vw',
            position: 'fixed',
            top: 0,
            left: 0,
            opacity: stage === 'done' || stage === 'textFreezeFrame' ? 0.5 : 0
          }} />
          <Video
            elementProps={{
              src: 'https://s3.amazonaws.com/gabetonimatrimony/Taketheplungecartoon640.mov',
              loop: false,
              muted: true,
              ref: this.videoRef,
              className: 'fullscreen-video',
              style: {
                ...plungeVideoDefaultStyle,
                ...plungeVideoStyles[stage]
              }
            }}
            nodeProps={videoNodeProps} />
          {(stage === 'playing' || stage === 'textFreezeFrame' || stage === 'done') &&
            <div style={{position: 'relative'}}>
              <PitStopHeader>
                <span style={{visibility: stage === 'done' ? 'visible' : 'hidden'}}>
                  RSVP
                </span>
              </PitStopHeader>
              <Container as='h2' textAlign='center' style={{visibility: stage === 'done' || stage === 'textFreezeFrame' ? 'visible' : 'hidden'}}>
                Celebrate with Toni & Gabe as they take the plunge
              </Container>
            </div>
          }
          {stage === 'done' && <RSVPFormContainer />}
        </div>
      </Transition>
    );
  }
}

export default RSVP;
