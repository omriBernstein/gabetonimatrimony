import React, {Component} from 'react';
import {Image, Grid, Container} from 'semantic-ui-react';
import Carousel from 'react-slick';

import ProposalVideoContainer from '../containers/ProposalVideo.container';
import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';

const StoryText = () => (
  <Container>
    <Image
      src='https://s3.amazonaws.com/gabetonimatrimony/mount-bonnell-nook.jpg'
      shape='rounded'
      className='white-border'
      size='medium'
      floated='left'
      alt='Caption: Our secret-not-so-secret nook on Mount Bonnell overlooking Lake Austin and the 360 bridge.' />
    <Container as='h3'>
      On the early morning of April 23rd 2016, Gabriel and Toni had planned to shoot a "music video" at their favourite spot atop Mount Bonnell, overlooking Lake Austin, to test out Gabe's fancy pants field recorder. This kind of behavior was nothing out of the ordinary for this acoustics nerd. What was out of the ordinary  was how patient Gabriel was, while waiting for Toni to get dolled up before heading out to their little nook on the cliff.
    </Container>
    <Container as='h3'>
      They arrived at their spot and hooray, it was empty! Gabriel set up his Gopro, recorder, and whipped out his guitar. Meanwhile, Toni was more interested in eating her oranges than starring in this impromptu music video, the big citrus gorb! He instructed her to look out at the horizon, all artsy-like, to make sure she couldn't see what was about to happen...
    </Container>
    <Container as='h3'>
      He had began to play a familiar song, one that he had made and been playing to her for the last year. To Toni's knowledge, this song was a work in progress and had no lyrics yet. Disclaimer: Gabe had actually written the song a year before and had been planting the seed for this big day. Also, there were several of their friends hiding behind bushes holding a violin, cello, congo, and more cameras.
    </Container>
    <Container as='h3'>
      Watch Toni reaction to what the lyrics revealed...
    </Container>
    <br />
  </Container>
);

const StoryVideo = () => (
  <Container as='p'>
    <ProposalVideoContainer />
  </Container>
);

const carouselImageUrls = [
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-01.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-02.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-03.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-04.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-05.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-06.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-07.png',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-08.png',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-09.png',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-10.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-11.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-12.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/engagement-carousel/engagement-carousel-13.jpg',
];

const StoryPhotoCarousel = () => (
  <Container style={{marginBottom: '3em'}}>
    <Carousel
      accessibility
      arrows
      variableWidth
      centerMode
      centerPadding='1em'
      infinite={false}
      dots
      dotsClass='slick-dots carousel-dots'
      draggable
      slidesToShow={1}
      slidesToScroll={1}
      swipeToSlide
      focusOnSelect
      className='carousel-tighten'>
      {carouselImageUrls.map(url => (
        <Image
          key={url}
          src={url}
          className='white-border'
          style={{
            height: 'auto',
            maxHeight: '200px',
            maxWidth: 'calc(100vw - 10em)',
            margin: '10px'
          }}
          shape='rounded' />
      ))}
    </Carousel>
  </Container>
);

class EngagementStory extends Component {
  render () {
    return (
      <Container>
        <PitStopHeader content='The Proposal' />
        <Grid style={{marginTop: '2em'}}>
          <Grid.Row columns={2}>
            <Grid.Column mobile={16} computer={8}>
              <StoryText />
            </Grid.Column>
            <Grid.Column mobile={16} computer={8}>
              <StoryVideo />
              <StoryPhotoCarousel />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <BobbingDownArrow />
      </Container>
    );
  }
}

export default EngagementStory;
