import React from 'react';
import {Container, Icon} from 'semantic-ui-react';

import {scrollNavigatingEnabled} from '../constants';

const BobbingDownArrow = () => (
  <Container textAlign='center' style={{marginBottom: '80px', fontSize: '14px'}}>
    {scrollNavigatingEnabled &&
      <Icon name='angle double down' size='huge' className='icons-font bob' />
    }
  </Container>
);

export default BobbingDownArrow;
