import React, {Component} from 'react';

import GatewayContainer from '../containers/Gateway.container';
import Home from './Home.component';
import RouteToPitStopContainer from '../containers/RouteToPitStop.container';
import DimmingLoader from './DimmingLoader.component';
import {excludesRegistry} from '../constants';

class Entry extends Component {
  constructor () {
    super();
    this.state = {
      loading: true
    };
  }
  componentWillMount () {
    const {loadMe, passGateway} = this.props;
    loadMe()
    .then(({payload: me}) => {
      this.setState({
        loading: false
      });
      if (me && me.hasOwnProperty('id')) {
        passGateway();
      } else if (excludesRegistry) {
        passGateway();
      }
    });
  }
  render () {
    const {loading} = this.state;
    if (loading) return <DimmingLoader />;
    const {passedGateway} = this.props;
    return (
      <div>
        {passedGateway ?
          <div>
            <Home />
            <RouteToPitStopContainer />
          </div>
          :
          <GatewayContainer />
        }
      </div>
    );
  }
}

export default Entry;
