import React, {Component} from 'react';
import {Header, Container, Image, Divider, Icon} from 'semantic-ui-react';

import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';

class EventDetails extends Component {
  render () {
    return (
      <Container style={{fontSize: '1.5rem'}}>
        <PitStopHeader content='Event Details' />
        <Header as='h2' textAlign='center'>Ballybeg House, Ballinglen, Co. Wicklow</Header>
        <Container textAlign='center'>
          <Image
            shape='rounded'
            centered
            className='white-border'
            src='https://s3.amazonaws.com/gabetonimatrimony/event-location-ballybeg-house.png' />
          <Header as='h3' textAlign='center'>
            <a href='http://www.ballybeg.ie/' target='_blank' rel='noopener noreferrer'>www.ballybeg.ie</a>
          </Header>
        </Container>
        <Header as='h2' textAlign='center'>
          Tuesday, May 29th 2018
        </Header>
        <Header as='h2' textAlign='center'>
          2:00PM
        </Header>
        <Divider horizontal>:</Divider>
        <Container textAlign='center'>
          <div style={{display: 'inline-block', position: 'relative'}}>
            <Image
              centered
              src='https://s3.amazonaws.com/gabetonimatrimony/event-ireland.png'
              style={{
                maxHeight: '380px'
              }} />
            <a
              href='https://goo.gl/maps/RgqrHC5AWEt'
              target='_blank'
              rel='noopener noreferrer'
              style={{textDecoration: 'none'}}>
              <Icon
                name='star'
                color='yellow'
                className='pulse icons-font'
                style={{
                  position: 'absolute',
                  left: '76%',
                  top: '61%'
                }} />
            </a>
          </div>
        </Container>
        {/*<div
          style={{
            background: 'url(https://s3.amazonaws.com/gabetonimatrimony/event-ireland.png) no-repeat center center fixed',
            backgroundSize: 'contain',
            backgroundAttachment: 'initial',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            flexWrap: 'wrap',
            position: 'relative'
          }}>
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>12pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>Pre-ceremony Jitters and Seating</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>1pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>The I Do’s</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>2pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>“5 o’clock somewhere excuse” prevails</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>4pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>Speeches/toasts and Grub Time</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>5pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>Cake Demolishing (yes, it gets it’s own category)</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>530pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>First Dances and Bouquet Chuck</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>6pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>Party ’til the cows come home</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>8pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>CHIPPY</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div style={{visibility: 'hidden', textAlign: 'right', flex: 1}}>9pm</div>
          <div style={{visibility: 'hidden', textAlign: 'center', padding: '0 1em'}}>-</div>
          <div style={{visibility: 'hidden', textAlign: 'left', flex: 2.5}}>Campfire and S’mores</div>
          <div style={{visibility: 'hidden', width: '100vw', margin: '0.5em 0'}} />
          <div
            className='pulse'
            style={{
              position: 'absolute',
              height: '100%',
              width: '100%',
              top: 0,
              left: 0,
              transformOrigin: '75% 64%',
              background: 'url(https://s3.amazonaws.com/gabetonimatrimony/event-location-star.png) no-repeat center center fixed',
              backgroundSize: 'contain',
              backgroundAttachment: 'initial',
            }} />
        </div>*/}
        <Divider horizontal>:</Divider>
        <Header as='h3' textAlign='center'>
          For recommended nearby accommodations for our lovely guests <a href='http://www.ballybeg.ie/wp-content/uploads/2016/10/Accommodation.pdf' target='_blank' rel='noopener noreferrer'>click here</a>
        </Header>
        <Header as='h3' textAlign='center'>
          For driving directions and a google map of Ballybeg House <a href='http://www.ballybeg.ie/more-information/directions/' target='_blank' rel='noopener noreferrer'>click here</a>
        </Header>
        <BobbingDownArrow />
      </Container>
    );
  }
}

export default EventDetails;
