import React, {Component} from 'react';

import Video from './Video.component';

class ProposalVideo extends Component {
  constructor () {
    super();
    this.playing = false;
  }
  videoRef = (node) => {
    if (node) {
      this._node = node;
      this.videoMount();
    } else {
      this.videoUnmount();
    }
  }
  videoMount () {
    this._node.addEventListener('playing', this.onPlaying);
    this._node.addEventListener('ended', this.onNotPlaying);
    this._node.addEventListener('pause', this.onNotPlaying);
  }
  videoUnmount () {
    this._node.removeEventListener('playing', this.onPlaying);
    this._node.removeEventListener('ended', this.onNotPlaying);
    this._node.removeEventListener('pause', this.onNotPlaying);
  }
  onPlaying = () => {
    this.playing = true;
    this.props.pauseBackgroundAudio();
  }
  onNotPlaying = () => {
    if (this.props.windowHasFocus) {
      this.playing = false;
      this.props.playBackgroundAudio();
    }
  }
  render () {
    const {windowHasFocus} = this.props;
    return (
      <Video
        playing={this.playing && windowHasFocus}
        elementProps={{
          src: 'https://s3.amazonaws.com/gabetonimatrimony/engagement640.mov',
          style: {width: '100%'},
          controls: true,
          ref: this.videoRef
        }} />
    );
  }
}

export default ProposalVideo;
