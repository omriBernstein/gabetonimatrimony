import React, {Component} from 'react';
import {Header, Container, Grid, Image, Divider} from 'semantic-ui-react';

import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';

class Attire extends Component {
  render () {
    return (
      <Container>
        <PitStopHeader content='Attire' />
        <Header as='h2' textAlign='center'>Semi-formal / Dressy Casual</Header>
        <Grid textAlign='center'>
          <Grid.Row columns={2}>
            <Grid.Column mobile={16} computer={8}>
              <Divider style={{fontSize: '1.8em', lineHeight: '1.8em'}} horizontal>He should wear</Divider>
              <div style={{height: '45vh'}}>
                <Image
                  centered
                  style={{height: '100%'}}
                  src='https://s3.amazonaws.com/gabetonimatrimony/attire-him.png' />
              </div>
              <h3>A suit and tie.</h3>
            </Grid.Column>
            <Grid.Column mobile={16} computer={8}>
              <Divider style={{fontSize: '1.8em', lineHeight: '1.8em'}} horizontal>She should wear</Divider>
              <div style={{height: '45vh'}}>
                <Image
                  centered
                  style={{height: '100%'}}
                  src='https://s3.amazonaws.com/gabetonimatrimony/attire-her.png' />
              </div>
              <h3>A spring dress or a dressy skirt and top.</h3>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Container>
              <Divider style={{fontSize: '1.8em', lineHeight: '1.8em', marginTop: '-0.5em'}} horizontal>Everybody</Divider>
            </Container>
            <Container as='h3' textAlign='center'>
              It’s Ireland...it will get chilly and rain at any given moment! Bring something warm.
            </Container>
            <Container as='h3' textAlign='center'>
              Advice for the ladies: The outdoor ceremony may be on grass (weather permitting) and the terrain around Ballybeg may be stony and uneven. We’d advise flats, wedges or sandals! Your feetsies will thank us and their party life will be extended.
            </Container>
          </Grid.Row>
        </Grid>
        <Header as='h3' textAlign='center'>
          <a href='https://www.theknot.com/content/wedding-guest-attire-cheat-sheet' target='_blank' rel='noopener noreferrer'>(cheatsheet)</a>
        </Header>
        <BobbingDownArrow />
      </Container>
    );
  }
}

export default Attire;
