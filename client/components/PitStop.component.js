import React, {Component} from 'react';
import debounce from 'lodash.debounce';
import elementResizeEvent from 'element-resize-event';
import browser from 'detect-browser';

import {pitStops, scrollNavigatingEnabled, debounceTime} from '../constants';

const stopTimes = Array.from(pitStops(true).keys());

const pitStopStyle = {
  position: 'absolute',
  zIndex: 1,
  left: 0,
  right: 0
};

class PitStop extends Component {
  constructor () {
    super();
    if (browser.name !== 'safari') {
      this.setForwardsTrue = debounce(this.setForwardsTrue, debounceTime, {
        loading: true,
        trailing: true
      });
      this.setBackwardsTrue = debounce(this.setBackwardsTrue, debounceTime, {
        loading: true,
        trailing: true
      });
    }
  }
  componentDidUpdate (prevProps) {
    if (prevProps.currentStop !== this.props.currentStop) {
      window.scrollTo(0, 0);
      if (this._node) {
        setTimeout(() => {
          this.setDeltaActive(this._node.scrollHeight);
        }, 50);
      }
    }
  }
  captureNode = (node) => {
    if (node) {
      this._node = node;
      this.setDeltaActive(this._node.scrollHeight);
      elementResizeEvent(this._node, () => {
        this.setDeltaActive(this._node.scrollHeight);
      });
      if (browser.name === 'safari') {
        this.deltaActiveUpdate();
      }
    } else {
      if (browser.name === 'safari') {
        clearTimeout(this._timerId);
      }
    }
  }
  deltaActiveUpdate () {
    if (this._node) {
      this.setDeltaActive(this._node.scrollHeight);
    }
    this._timerId = setTimeout(this.deltaActiveUpdate, 50);
  }
  reachedTop () {
    return window.pageYOffset === 0;
  }
  reachedBottom (height) {
    return window.pageYOffset + window.innerHeight >= height;
  }
  setForwardsTrue = () => {
    this.props.setForwards(true);
  }
  setForwardsFalse = () => {
    this.props.setForwards(false);
  }
  setBackwardsTrue = () => {
    this.props.setBackwards(true);
  }
  setBackwardsFalse = () => {
    this.props.setBackwards(false);
  }
  setDeltaActive = (scrollHeight) => {
    if (this.reachedTop()) {
      this.setBackwardsTrue();
    } else {
      this.setBackwardsFalse();
    }
    if (this.reachedBottom(scrollHeight)) {
      this.setForwardsTrue();
    } else {
      this.setForwardsFalse();
    }
  }
  onTouchStart = ({touches}) => {
    this._touchBase = touches[0].pageY;
  }
  onTouchMove = (event) => {
    const touchDelta = this._touchBase - event.touches[0].pageY;
    this.onWheelOrTouch(event, touchDelta);
  }
  onWheel = (event) => {
    this.onWheelOrTouch(event, event.deltaY);
  }
  onWheelOrTouch (event, deltaY) {
    if (!scrollNavigatingEnabled) return;
    const height = this._node.scrollHeight;
    this.setDeltaActive(height);
    if ((this.reachedTop() && deltaY < 0) || (this.reachedBottom(height) && deltaY > 0)) {
      event.preventDefault();
    }
  }
  render () {
    const {currentStop, loading, isInvited} = this.props;
    const currentTime = stopTimes[currentStop];
    return (!loading &&
      <div
        style={pitStopStyle}
        ref={this.captureNode}
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onWheel={this.onWheel}>
        {stopTimes.map(time => (
          <div
            key={time}
            style={{
              display: time === currentTime ? 'block' : 'none',
              marginTop: '2em',
              marginBottom: 'calc(59px)'
            }}>
            {React.createElement(pitStops(isInvited).get(time), {
              active: time === currentTime
            })}
          </div>
        ))}
      </div>
    );
  }
}

export default PitStop;
