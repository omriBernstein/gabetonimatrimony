import React, {Component} from 'react';
import {Header, Container, Form, Input, TextArea, Button, Grid, Icon, Divider} from 'semantic-ui-react';

class RSVPForm extends Component {
  constructor (props) {
    super(props);
    this.state = this.calculateInitialState();
    this.guestNameChangeHandlers = this.state.data.guestNames.map((_, index) => {
      return this.guestNameChangeHandler(index);
    });
  }
  calculateInitialState () {
    const {me: {email, allowedGuests, guestNames, foodRequirements, rsvpMessage, attending}} = this.props;
    const initial = {
      email: email || '',
      guestNames: [...(guestNames || [])],
      foodRequirements: foodRequirements || '',
      rsvpMessage: rsvpMessage || '',
      attending: attending
    };
    let index = allowedGuests;
    while (index--) {
      initial.guestNames[index] = initial.guestNames[index] || '';
    }
    return {
      data: initial,
      submitting: false,
      showSuccessfulSubmit: false
    };
  }
  guestNameChangeHandler = (index) => ({target: {value}}) => {
    const guestNames = this.state.data.guestNames.slice();
    guestNames.splice(index, 1, value);
    this.setState({
      data: {
        ...this.state.data,
        guestNames
      }
    });
  }
  handleChange = ({target: {name, value}}) => {
    this.setState({
      data: {
        ...this.state.data,
        [name]: name === 'email' ? value.trim() : value
      }
    });
  }
  handleSubmit = (event) => {
    const {updateMe, me} = this.props;
    event.preventDefault();
    this.setState({
      submitting: true
    });
    updateMe(me.id, this.state.data)
    .then(() => {
      this.setState({
        showSuccessfulSubmit: true
      });
      setTimeout(() => {
        this.setState({
          showSuccessfulSubmit: false
        });
      }, 2000);
    })
    .catch(console.error)
    .then(() => {
      this.setState({
        submitting: false
      });
    });
  }
  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  }
  attendingToggler = (value) => () => {
    const {updateMe, me} = this.props;
    this.setState({
      data: {
        ...this.state.data,
        attending: this.state.data.attending === value ? null : value
      }
    }, () => {
      updateMe(me.id, this.state.data)
      .catch(console.error);
    });
  }
  render () {
    const {me: {allowedGuests}} = this.props;
    const {
      data: {email, guestNames, foodRequirements, rsvpMessage, attending},
      submitting,
      showSuccessfulSubmit
    } = this.state;
    const filledOut = guestNames.filter(name => name.trim() !== '').length;
    return (
      <Container style={{marginBottom: '6em'}}>
        <Container style={{position: 'relative'}}>
          <Divider hidden />
          <Grid style={{alignItems: 'center'}} textAlign='center'>
            <Button
              size='big'
              style={{margin: 0}}
              color={attending === true ? 'olive' : 'grey'}
              onClick={this.attendingToggler(true)}>
              Attending
            </Button>
            <Grid.Column className='full-width pt-tiny' only='mobile' />
            <span style={{margin: '0 1em'}}>OR</span>
            <Grid.Column className='full-width pt-tiny' only='mobile' />
            <Button
              size='big'
              style={{margin: 0}}
              color={attending === false ? 'olive' : 'grey'}
              onClick={this.attendingToggler(false)}>
              <span style={{textDecoration: 'underline'}}>Not</span> attending
            </Button>
          </Grid>
          <Divider hidden />
        </Container>
        {attending !== null &&
          <div>
            <Divider horizontal>:</Divider>
            {attending === true &&
              <Header as='h2' textAlign='center' style={{position: 'relative'}}>
                Be a wee gem and fill this stuff out for us, would ya??
              </Header>
            }
            <br />
            <Form style={{fontSize: '1.1em'}}>
              <Grid verticalAlign='middle'>
                {attending === true && [
                  <Grid.Column className='min-width-content flex-1 pb0' key='a'>
                    <Header content='Email' textAlign='right' className='pt-slight mobile-left' />
                  </Grid.Column>,
                  <Grid.Column className='full-width p0' only='mobile' key='b' />,
                  <Grid.Column className='flex-3' key='c'>
                    <Input className='initial-font' fluid>
                      <input
                        type='text'
                        value={email}
                        name='email'
                        onChange={this.handleChange}
                        onKeyPress={this.handleKeyPress}
                        style={{fontSize: '1.1em', lineHeight: '1.6em'}} />
                    </Input>
                  </Grid.Column>,
                  <Grid.Column className='full-width p0' key='d' />,
                  <Grid.Column className='min-width-content flex-1 pb0' key='e' />,
                  <Grid.Column className='full-width p0' only='mobile' key='f' />,
                  <Grid.Column className='flex-3' key='g'>
                    <Header as='h3' textAlign='left'>
                      {allowedGuests <= 1 ?
                        <div>We have reserved a seat for you</div>
                        :
                        <div>
                          We have reserved {allowedGuests} seats in your honor, including you. If a guest is not attending, please leave that guest's name blank in the input below.
                        </div>
                      }
                    </Header>
                  </Grid.Column>,
                  <Grid.Column className='full-width p0' key='h' />,
                  guestNames.map((name, index) => ([
                    <Grid.Column key={`${index}a`} className='min-width-content flex-1 pb0 pt-slight'>
                      <Header textAlign='right' className='pt-slight mobile-left'>
                        {allowedGuests > 1 ? `Guest #${index + 1}` : 'Your'} name
                      </Header>
                    </Grid.Column>,
                    <Grid.Column key={`${index}b`} className='full-width p0' only='mobile' />,
                    <Grid.Column key={`${index}c`} className='flex-3 pb0 pt-slight'>
                      <Input className='initial-font' fluid>
                        <input
                          type='text'
                          value={name}
                          onChange={this.guestNameChangeHandlers[index]}
                          onKeyPress={this.handleKeyPress}
                          style={{fontSize: '1.1em', lineHeight: '1.6em'}} />
                      </Input>
                    </Grid.Column>,
                    <Grid.Column key={`${index}d`} className='full-width p0' />
                  ])),
                  <Grid.Column className='full-width p0' key='i' />,
                  <Grid.Column className='min-width-content flex-1 pb0' key='j' />,
                  <Grid.Column className='full-width p0' only='mobile' key='k' />,
                  <Grid.Column className='flex-3' key='l'>
                    <Header as='h3' textAlign='left'>
                      {allowedGuests > 1 &&
                          <div>
                            Based on how many name inputs are filled in, you have confirmed {filledOut} of your {allowedGuests} seats.
                          </div>
                        }
                    </Header>
                  </Grid.Column>,
                  <Grid.Column className='full-width p0' key='m' />,
                  <Grid.Column className='min-width-content flex-1 pb0' key='n'>
                    <Header content='Dietary restrictions' textAlign='right' className='pt-slight mobile-left' />
                  </Grid.Column>,
                  <Grid.Column className='full-width p0' only='mobile' key='o' />,
                  <Grid.Column className='flex-3' key='p'>
                    <TextArea
                      className='initial-font'
                      rows={2}
                      value={foodRequirements}
                      name='foodRequirements'
                      onChange={this.handleChange}
                      onKeyPress={this.handleKeyPress}
                      style={{fontSize: '1.1em', lineHeight: '1.6em'}} />
                  </Grid.Column>,
                  <Grid.Column className='full-width p0' key='q' />,
                ]}
                <Grid.Column className='min-width-content flex-1 pb0'>
                  <Header content='Any words for the bride and groom?' textAlign='right' className='pt-slight mobile-left' />
                </Grid.Column>
                <Grid.Column className='full-width p0' only='mobile' />
                <Grid.Column className='flex-3'>
                  <TextArea
                    className='initial-font'
                    rows={5}
                    value={rsvpMessage}
                    name='rsvpMessage'
                    onChange={this.handleChange}
                    onKeyPress={this.handleKeyPress}
                    style={{fontSize: '1.1em', lineHeight: '1.6em'}} />
                </Grid.Column>
                <Grid.Column className='full-width p0' />
                <Grid.Row className='full-width'>
                  <Grid.Column>
                    <Button
                      floated='right'
                      size='big'
                      color='olive'
                      onClick={this.handleSubmit}
                      loading={submitting}
                      disabled={submitting || showSuccessfulSubmit}
                      style={{
                        width: '6.5em'
                      }}>
                        {showSuccessfulSubmit ?
                          <Icon name='checkmark' className='icons-font' />
                          :
                          'submit'
                        }
                      </Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Form>
          </div>
        }
      </Container>
    );
  }
}

export default RSVPForm;
