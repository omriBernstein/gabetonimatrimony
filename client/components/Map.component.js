import React from 'react';
import {GoogleMap} from 'react-google-maps';
import ScriptjsLoader from 'react-google-maps/lib/async/ScriptjsLoader';

import {googleMapStyle} from '../constants';

const Map = () => (
  <ScriptjsLoader
    hostname={'maps.googleapis.com'}
    pathname={'/maps/api/js'}
    query={{v: '3.27', libraries: 'geometry,drawing,places'}}
    loadingElement={
      <div>Loading...</div>
    }
    containerElement={
      <div style={{height: '80vh'}} />
    }
    googleMapElement={
      <GoogleMap
        defaultZoom={16}
        defaultCenter={{lat: 52.6744461, lng: -8.6323632}}
        defaultOptions={{
          styles: googleMapStyle,
          scrollwheel: false
        }} />
    }
  />
);

export default Map;
