import React from 'react';
import {Provider} from 'react-redux';
import {HashRouter} from 'react-router-dom';
import store from '../store';

import EntryContainer from '../containers/Entry.container';

const Root = () => (
  <Provider store={store}>
    <HashRouter>
      <EntryContainer />
    </HashRouter>
  </Provider>
);

export default Root;
