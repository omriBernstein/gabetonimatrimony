import React, {Component} from 'react';
import browser from 'detect-browser';
import createPlayer from 'web-audio-player';

class Audio extends Component {
  constructor (props) {
    super();
    this.playing = false;
    if (browser.name === 'ios') {
      this.player = createPlayer(props.src, {
        loop: props.loop
      });
      this.player.on('load', this.onLoad);
    }
  }
  componentWillReceiveProps (nextProps) {
    this.maybePlayOrPause(nextProps.playing);
    if (this.volume !== nextProps.volume) {
      this.setVolume(nextProps.volume);
    }
  }
  maybePlayOrPause (shouldBePlaying) {
    if (!this.playing && shouldBePlaying) {
      this.play();
    }
    if (this.playing && !shouldBePlaying) {
      this.pause();
    }
  }
  audioRef (node) {
    if (node) {
      this._node = node;
      this.onLoad();
    }
  }
  onLoad = () => {
    this.setVolume(this.props.volume || 1);
    this.maybePlayOrPause(this.props.playing);
  }
  setVolume (volume) {
    if (typeof volume !== 'number') return;
    this.volume = volume;
    if (this.player) {
      this.player.volume = volume;
    } else {
      this._node.volume = volume;
    }
  }
  computeChildProps () {
    const childProps = {
      ...this.props,
      ref: (node) => {
        this.audioRef(node);
        if (typeof this.props.ref === 'function') {
          this.props.ref(node);
        }
      }
    };
    delete childProps.playing;
    delete childProps.volume;
    return childProps;
  }
  play () {
    this.playing = true;
    if (this.player) {
      this.player.play();
    } else {
      this._node.play();
    }
  }
  pause () {
    if (this.player) {
      this.player.pause();
    } else {
      this._node.pause();
    }
    this.playing = false;
  }
  render () {
    if (browser.name === 'ios') return <div />;
    const childProps = this.computeChildProps();
    return (
      <audio {...childProps} />
    );
  }
}

export default Audio;
