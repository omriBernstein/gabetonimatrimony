import React, {Component} from 'react';

import {pitStopUrls} from '../constants';
import {roadVideoDurationIsSet} from '../promises';

class RouteToPitStop extends Component {
  constructor () {
    super();
    this.justSetStopFromUrl = false;
    this.justSetUrlFromStop = false;
  }
  componentDidMount () {
    const {location} = this.props;
    roadVideoDurationIsSet.then(() => {
      this.setStopFromUrl(location.pathname);
    });
  }
  componentWillReceiveProps (nextProps) {
    const {location, currentStop} = this.props;
    if (nextProps.location.pathname !== location.pathname) {
      if (this.justSetUrlFromStop) {
        this.justSetUrlFromStop = false;
      } else {
        this.setStopFromUrl(nextProps.location.pathname);
      }
    }
    const nextStop = nextProps.currentStop;
    if (nextStop !== currentStop) {
      if (this.justSetStopFromUrl) {
        this.justSetStopFromUrl = false;
        if (nextStop === null) {
          this.setUrlFromStop(nextStop);
        }
      } else {
        this.setUrlFromStop(nextStop);
      }
    }
  }
  setStopFromUrl (url) {
    const {isInvited, setStop, history} = this.props;
    const nextStop = pitStopUrls(isInvited).indexOf(url);
    if (nextStop === -1) {
      history.push(pitStopUrls(isInvited)[0]);
    } else {
      this.justSetStopFromUrl = true;
      setStop(nextStop);
    }
  }
  setUrlFromStop (stop) {
    const {isInvited, history, location} = this.props;
    const nextUrl = stop === null ? '/' : pitStopUrls(isInvited)[stop];
    if (location.pathname !== nextUrl) {
      this.justSetUrlFromStop = true;
      history.push(nextUrl);
    }
  }
  render () {
    return (
      <div style={{display: 'none'}} />
    );
  }
}

export default RouteToPitStop;
