import React, {Component} from 'react';

import Video from './Video.component';
import DimmingLoader from './DimmingLoader.component';
import oneTimeEventListener from '../utils/one-time-event-listener';

class Road extends Component {
  componentDidMount () {
    this._node.addEventListener('durationchange', this.onDurationChange);
    this._node.addEventListener('timeupdate', this.onTimeUpdate);
    oneTimeEventListener(this._node, 'canplaythrough', this.onCanPlayThrough);
  }
  componentWillUnmount () {
    this._node.removeEventListener('durationchange', this.onDurationChange);
    this._node.removeEventListener('timeupdate', this.onTimeUpdate);
  }
  captureNode = (node) => {
    this._node = node;
  }
  onCanPlayThrough = () => {
    this.props.roadLoaded();
  }
  onDurationChange = () => {
    const {setDuration} = this.props;
    setDuration(this._node.duration);
  }
  onTimeUpdate = () => {
    const {setCurrentTime} = this.props;
    setCurrentTime(this._node.currentTime);
  }
  render () {
    const {prospectiveTime, blurred, loading} = this.props;
    return (
      <div
        style={{
          position: 'fixed',
          height: '100%',
          width: '100%',
          top: 0,
          left: 0,
          backgroundColor: '#000'
        }}>
        {loading && <DimmingLoader />}
        <Video
          elementProps={{
            src: 'https://s3.amazonaws.com/gabetonimatrimony/IrishRoadCartoon640.mov',
            className: 'fullscreen-video',
            muted: true,
            style: {
              filter: blurred && 'blur(5px)',
              opacity: blurred ? 0.5 : 1,
              position: 'fixed',
              display: loading ? 'none' : 'initial'
            },
            ref: this.captureNode
          }}
          nodeProps={{
            currentTime: prospectiveTime
          }} />
        </div>
    );
  }
}

export default Road;
