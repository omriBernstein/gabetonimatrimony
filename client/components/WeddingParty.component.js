import React, {Component} from 'react';
import {Container, Grid, Image} from 'semantic-ui-react';

import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';

const WeddingPartyCard = ({title, imageUrl, firstName, lastName, shape = 'rounded'}) => (
  <div>
    <div>
      <Image
        centered
        src={imageUrl}
        shape={shape}
        className='white-border' />
    </div>
    <div style={{fontSize: '1.4em', lineHeight: '1.2em', margin: '0.4em'}}>
      {firstName}
      <br />
      {lastName}
    </div>
    <div style={{fontSize: '0.9em'}}>({title})</div>
  </div>
);

class WeddingParty extends Component {
  render () {
    return (
      <Container>
        <PitStopHeader content='Wedding Party' />
        <br />
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column>
              <Grid>
                <Grid.Row columns={1} centered>
                  <Grid.Column textAlign='center' mobile={16} computer={9}>
                    <WeddingPartyCard
                      title='Bride'
                      firstName='Toni'
                      lastName='Kane'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/bride.jpg' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2} centered>
                  <Grid.Column textAlign='center' mobile={16} computer={7}>
                    <WeddingPartyCard
                      title='Mother of the Bride'
                      firstName='Ann'
                      lastName='Stewart'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/bride-mother.jpg' />
                  </Grid.Column>
                  <Grid.Column textAlign='center' mobile={16} computer={7}>
                    <WeddingPartyCard
                      title='Brother of the Bride'
                      firstName='Stephen'
                      lastName='Kane'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/bride-brother.png' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={3} centered>
                  <Grid.Column textAlign='center' mobile={16} computer={5}>
                    <WeddingPartyCard
                      title='Maid of Honor'
                      firstName='Emma'
                      lastName='Nolan'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/bride-maid-of-honor.png' />
                  </Grid.Column>
                  <Grid.Column textAlign='center' mobile={16} computer={5}>
                    <WeddingPartyCard
                      title='Bridesmaid'
                      firstName='Christina'
                      lastName='Convey'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/bride-bridesmaid-christina.jpg' />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
            <Grid.Column>
              <Grid>
                <Grid.Row columns={1} centered>
                  <Grid.Column textAlign='center' mobile={16} computer={9}>
                    <WeddingPartyCard
                      title='Groom'
                      firstName='Gabriel'
                      lastName='Venegas'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/groom.jpg' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2} centered>
                  <Grid.Column textAlign='center' mobile={16} computer={7}>
                    <WeddingPartyCard
                      title='Mother of the Groom'
                      firstName='Magnolia'
                      lastName='Venegas'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/groom-mother.jpg' />
                  </Grid.Column>
                  <Grid.Column textAlign='center' mobile={16} computer={7}>
                    <WeddingPartyCard
                      title='Father of the Groom'
                      firstName='Jose'
                      lastName='Venegas'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/groom-father.jpg' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={3} centered>
                  <Grid.Column textAlign='center' mobile={16} computer={5}>
                    <WeddingPartyCard
                      title='Best Man'
                      firstName='Nick'
                      lastName='Dart'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/groom-best-man.jpg' />
                  </Grid.Column>
                  <Grid.Column textAlign='center' mobile={16} computer={5}>
                    <WeddingPartyCard
                      title='Groomsman'
                      firstName='Omri'
                      lastName='Bernstein'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/groom-groomsman-omri.jpg' />
                  </Grid.Column>
                  <Grid.Column textAlign='center' mobile={16} computer={5}>
                    <WeddingPartyCard
                      title='Groomsman'
                      firstName='Jorge'
                      lastName='Ferro'
                      imageUrl='https://s3.amazonaws.com/gabetonimatrimony/wedding-party/groom-groomsman-jorge.jpg' />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <BobbingDownArrow />
      </Container>
    );
  }
}

export default WeddingParty;
