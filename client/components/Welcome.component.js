import React, {Component} from 'react';
import {Container, Image, Icon} from 'semantic-ui-react';

import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';
import {scrollNavigatingEnabled} from '../constants';
import browser from 'detect-browser';

class Welcome extends Component {
  render () {
    return (
      <Container>
        <PitStopHeader content='Welcome!' />
        <Container style={{fontSize: '1.4em'}} textAlign='center'>Bien Venidos!</Container>
        <Container style={{fontSize: '1.4em'}} textAlign='center'>Bem Vindos!</Container>
        <Container style={{fontSize: '1.4em'}} textAlign='center'>Failte!</Container>
        <br />
        <Container as='p' textAlign='center'>
          <Image
            centered
            style={{
              maxWidth: '30%',
              maxHeight: '15em'
            }}
            src='https://s3.amazonaws.com/gabetonimatrimony/welcome-white-greenshadow.png' />
        </Container>
        <Container as='h3' textAlign='center'>
          Thanks for checking out our wee wedding website, created by our lovely Omri Bernstein. Enjoy {scrollNavigatingEnabled && '"scrolling down" '}the Irish coastline that we videoed while driving around Co. Donegal.
        </Container>
        <Container as='h3' textAlign='center'>
          We hope you find each "pit stop" fun and informative!
        </Container>
        <Container as='h3' textAlign='center'>
          {browser.name !== 'safari' && browser.name !== 'edge' && browser.name !== 'ios' &&
            <span>This site works best when using the Safari or Edge web browsers, but continue if you wish <Icon name='smile' size='large' className='icons-font' /></span>
          }
        </Container>
        <Container as='h3' textAlign='center'>
          {scrollNavigatingEnabled ?
            <span>Get scrolling...</span>
            :
            <span>Navigate by clicking the links in the menu below...</span>
          }
        </Container>
        <BobbingDownArrow />
      </Container>
    );
  }
}

export default Welcome;
