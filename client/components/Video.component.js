import React, {Component} from 'react';
import enableInlineVideo from 'iphone-inline-video';
import browser from 'detect-browser';

import oneTimeEventListener from '../utils/one-time-event-listener';
import fetchObjectUrl from '../utils/fetch-object-url';
import eachkv from '../utils/eachkv';

class Video extends Component {
  constructor () {
    super();
    this.playing = false;
    this._transformers = {
      currentTime: (newTime) => {
        if (!this._node || !this._node.duration) return newTime;
        return Math.min(Math.max(0, newTime), this._node.duration);
      }
    };
    this.state = {
      objectUrl: undefined,
      canSetNodeProps: false
    };
  }
  componentDidMount () {
    enableInlineVideo(this._node);
    if (!this.props.elementProps.autoPlay && browser.name === 'ios') {
      this.listenToStopAutoPlay();
    }
    if (browser.name === 'chrome') {
      fetchObjectUrl(this.props.elementProps.src)
      .then((objectUrl) => {
        this.setState({
          objectUrl
        });
      })
      .catch(console.error);
    }
  }
  listenToStopAutoPlay () {
    oneTimeEventListener(this._node, 'play', () => {
      this._node.pause();
      this._node.currentTime = 0;
    });
  }
  _assignNodeProps = (node) => {
    if (!this.props.nodeProps) return;
    eachkv(this.props.nodeProps, (k, v) => {
      if (this._transformers.hasOwnProperty(k)) {
        node[k] = this._transformers[k](v);
      } else {
        node[k] = v;
      }
    });
  }
  videoRef = (node) => {
    if (this.props.elementProps && this.props.elementProps.ref) {
      this.props.elementProps.ref(node);
    }
    if (node) {
      this._node = node;
      this._node.addEventListener('canplay', this.onCanPlay);
      this._node.addEventListener('canplaythrough', this.onCanPlayThrough);
      this._node.addEventListener('playing', this.onPlaying);
      this._node.addEventListener('ended', this.onNotPlaying);
      this._node.addEventListener('pause', this.onNotPlaying);
    } else {
      this._node.removeEventListener('canplay', this.onCanPlay);
      this._node.removeEventListener('canplaythrough', this.onCanPlayThrough);
      this._node.removeEventListener('playing', this.onPlaying);
      this._node.removeEventListener('ended', this.onNotPlaying);
      this._node.removeEventListener('pause', this.onNotPlaying);
    }
  }
  onCanPlay = () => {
    this.maybePlayOrPause(this.props.playing);
  }
  onCanPlayThrough = () => {
    setTimeout(() => {
      this.setState({
        canSetNodeProps: true
      });
    }, 50);
  }
  onPlaying = () => {
    this.playing = true;
  }
  onNotPlaying = () => {
    this.playing = false;
  }
  computeChildProps () {
    const childProps = {...this.props.elementProps};
    if (browser.name === 'chrome') {
      if (this.state.objectUrl === undefined) {
        delete childProps.src;
      } else {
        childProps.src = this.state.objectUrl;
      }
    }
    delete childProps.playing;
    return childProps;
  }
  componentWillReceiveProps (nextProps) {
    this.maybePlayOrPause(nextProps.playing);
  }
  maybePlayOrPause (shouldBePlaying) {
    if (shouldBePlaying === undefined || !this._node) return;
    if (!this.playing && shouldBePlaying) {
      this.play();
    }
    if (this.playing && !shouldBePlaying) {
      this.pause();
    }
  }
  play () {
    this.playing = true;
    this._node.play();
  }
  pause () {
    this._node.pause();
    this.playing = false;
  }
  render () {
    if (this.state.canSetNodeProps) {
      this._assignNodeProps(this._node);
    }
    const childProps = this.computeChildProps();
    return (
      <video
        playsInline
        preload='auto'
        autoPlay={browser.name === 'ios'}
        {...childProps}
        ref={this.videoRef} />
    );
  }
}

export default Video;
