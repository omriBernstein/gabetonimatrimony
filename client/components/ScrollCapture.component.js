import React, {Component} from 'react';

import {scalePow} from 'd3-scale';
import throttle from 'lodash.throttle';
import debounce from 'lodash.debounce';

import store from '../store';
import {
  scrollNavigatingEnabled,
  throttleTime,
  debounceTime,
  deltaEffect
} from '../constants';

const wheelDeltaToTimeDelta = scalePow()
.exponent(0.5)
.domain([-300, 300])
.range([-deltaEffect, deltaEffect])
.clamp(true);

class ScrollCapture extends Component {
  constructor () {
    super();
    this.applyWheelDelta = throttle(this.applyWheelDelta, throttleTime);
    this.stopStickingViaWheel = debounce(this._stopSticking, debounceTime);
    this.stopStickingViaTouch = debounce(this._stopSticking, debounceTime * 2);
  }
  onWheel = (event) => {
    this.stopStickingViaWheel();
    this.onWheelOrTouch(event, event.deltaY);
  }
  onWheelOrTouch (event, delta) {
    const {deltaActive: {forwards, backwards}, road: {stuck}} = store.getState();
    if (stuck) {
      event.preventDefault();
      return;
    }
    const shouldApply = delta > 0 ? forwards : backwards;
    if (shouldApply) {
      event.preventDefault();
      this.props.setForwards(true);
      this.props.setBackwards(true);
      this.applyWheelDelta(delta);
    }
  }
  applyWheelDelta = (delta) => {
    const {applyDelta} = this.props;
    applyDelta(wheelDeltaToTimeDelta(delta));
  }
  _stopSticking = () => {
    const {unstick} = this.props;
    unstick();
  }
  onTouchStart = ({touches}) => {
    this._touchBase = touches[0].pageY;
  }
  onTouchMove = (event) => {
    const touchDelta = this._touchBase - event.touches[0].pageY;
    this.stopStickingViaTouch();
    this.onWheelOrTouch(event, touchDelta);
  }
  render () {
    const {children} = this.props;
    if (!scrollNavigatingEnabled) return children;
    return (
      <div
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onWheel={this.onWheel}>
        {children}
      </div>
    );
  }
}

export default ScrollCapture;
