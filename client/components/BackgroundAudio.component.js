import React, {Component} from 'react';

import Audio from './Audio.component';

class BackgroundAudio extends Component {
  render () {
    const {playing, windowHasFocus} = this.props;
    return (
      <Audio
        src='https://s3.amazonaws.com/gabetonimatrimony/background-music-0-1.mp3'
        playing={playing && windowHasFocus}
        loop
        style={{
          display: 'hidden'
        }} />
    );
  }
}

export default BackgroundAudio;
