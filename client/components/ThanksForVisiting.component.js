import React from 'react';
import {Container} from 'semantic-ui-react';

import PitStopHeader from './PitStopHeader.component';
import {excludesRegistry} from '../constants';

const ThanksForVisiting = () => (
  <Container>
    <PitStopHeader content='Thanks for Visiting' />
    <Container as='h2' textAlign='center'>
      <div
        style={{
          display: 'flex',
          height: 'calc(70vh - 3em)',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
        <div>
          {excludesRegistry ?
            `You've reached the end of the road, thanks for visiting!`
            :
            `Without an access code, you've reached the end of the road! Remember to check your wedding invitation for an access code.`
          }
        </div>
      </div>
    </Container>
  </Container>
);

export default ThanksForVisiting;
