import React, {Component} from 'react';
import {Container, Image, Divider} from 'semantic-ui-react';
import Carousel from 'react-slick';

import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';

const carouselImageUrls = [
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-01.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-02.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-03.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-04.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-05.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-06.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-07.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-08.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-09.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-10.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-11.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-12.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-13.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-14.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-15.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-16.jpg',
  'https://s3.amazonaws.com/gabetonimatrimony/our-story-carousel/our-story-17.jpg'
];

const OurStoryPhotoCarousel = () => (
  <Container style={{marginBottom: '3em'}}>
    <Carousel
      accessibility
      arrows
      variableWidth
      centerMode
      centerPadding='1em'
      infinite={false}
      dots
      dotsClass='slick-dots carousel-dots'
      draggable
      slidesToShow={1}
      slidesToScroll={1}
      swipeToSlide
      focusOnSelect
      className='carousel-tighten'>
      {carouselImageUrls.map(url => (
        <Image
          key={url}
          src={url}
          className='white-border'
          style={{
            height: 'auto',
            maxHeight: '200px',
            maxWidth: 'calc(100vw - 10em)',
            margin: '10px'
          }}
          shape='rounded' />
      ))}
    </Carousel>
  </Container>
);

class OurStory extends Component {
  render () {
    return (
      <Container>
        <PitStopHeader content='Our Story' />
        <Container as='p' textAlign='center'>
          <Image
            centered
            className='white-border'
            style={{
              width: '50%'
            }}
            src='https://s3.amazonaws.com/gabetonimatrimony/first-date.jpeg' />
        </Container>
        <Container as='h3' textAlign='center'>
          Ok, so picture this for our first date: Gabriel swooning Toni with his bachata hips dancing the night away in a fancy ballroom. There was twirling, dipping and lengthy eye contact. But wait! A comedy show downtown? OK! Just when Toni thought the night was over, back to the dance hall they went and danced until the wee small hours. He dropped her home in a yellow cab with a kiss on the cheek. Hand on heart, Toni literally slid down the inside of her front door like in the chic-flicks.
        </Container>
        <Divider
          horizontal
          style={{
            marginTop: '2em',
            fontSize: '1.8em'
          }}>
          <span style={{lineHeight: '1.3em'}}>November 2013</span>
        </Divider>
        <Container as='h3' textAlign='center'>
          Back Story: You know the love is true when your hubby-to-be buys you a red granny cart. Yup, Gabriel had bought Toni a red shopping cart for her 21st birthday after hearing her complain about carrying heavy grocery bags in 100-degree heat. You know, the kind that you see the senile pushing around the park for no reason at all with big tires for wheels, exactly. That feeling you're feeling right now? PURE JEALOUSY. Anyway, she LOVED it, obviously, no matter what she tells you AND she could now bring home the big jar of jalapenos without her arms falling off. And this, ladies and gentlemen, lead directly to Toni professing her love to Gabriel just two month later. Fast-forward a few months: After the annual Austin Celtic Festival, Toni and Gabe walked home hand-in-hand talking about everything under the sun...including the red shopping cart and the big jar of jalapenos! Toni couldn't hold it any longer, she busted out laughing in the middle of the street (30o18'42.1"N 97o 43'21.7"W)...and out came the words "I love you." Another back-story: It only took Gabriel three days after officially dating to say these words. The guy knows what he wants!
        </Container>
        <Divider
          horizontal
          style={{
            marginTop: '2em',
            fontSize: '1.8em'
          }}>
          <span style={{lineHeight: '1.3em'}}>December 2013</span>
        </Divider>
        <Container as='h3' textAlign='center'>
          Ahh Boston, their first Christmas together. Night-skiing in a toasty -40 degrees, getting caught in a blizzard that stranded them in New Hampshire for a night, and being force fed McDonalds because it was LITERALLY the only establishment open in the entire state of New Hampshire, are all events that can really bring a couple together! Contrary to how it sounds, this trip was perfect. As Bob Ross would say, "there's no such thing as a mistake, just happy accidents."
        </Container>
        <Divider
          horizontal
          style={{
            marginTop: '2em',
            fontSize: '1.8em'
          }}>
          <span style={{lineHeight: '1.3em'}}>August 2014<br />to July 2015</span>
        </Divider>
        <Container as='h3' textAlign='center'>
          The dark lonesome days; a six-hour time difference filled with excessive Face-Time, filling the void with a pet hedgehog, and 365 days of waking up to sweet morning messages. Toni and Gabriel persevered their year of long-distance dating, cutting through time like a knife through room temperature butter. After a year of living on two different continents, Toni finally arrived back in Austin and they pledged to never spend time apart again.
        </Container>
        <OurStoryPhotoCarousel />
        <BobbingDownArrow />
      </Container>
    );
  }
}

export default OurStory;
