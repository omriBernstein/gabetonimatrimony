import React, {Component} from 'react';
import {
  Header,
  Modal,
  Container,
  Input,
  Button,
  Divider,
  Message
} from 'semantic-ui-react';
import browser from 'detect-browser';

class Gateway extends Component {
  componentDidMount () {
    this._node.focus();
  }
  captureNode = (node) => {
    this._node = node;
  }
  handleKeyPress = (event) => {
    if (event.key === 'Enter' && browser.name !== 'ios') {
      this.props.onSubmit();
    }
  }
  render () {
    const {
      accessCode,
      errors,
      onSubmit,
      onChange,
      showValidStatus,
      checking,
      continueOn
    } = this.props;
    const isValid = errors.length === 0;
    return (
      <div style={{
        height: '100vh',
        width: '100vw',
        background: 'url(https://s3.amazonaws.com/gabetonimatrimony/gateway-background.png) no-repeat center center fixed',
        backgroundSize: 'cover'
      }}>
        <Modal open dimmer={false} size='small'>
          <Header
            textAlign='center'
            style={{
              backgroundColor: '#556B2F',
              color: 'white'
            }}>
            Howdy partner, you've found yourself at Gabe & Toni's wedding website
          </Header>
          <Modal.Content>
            <Modal.Description>
              <Header textAlign='center'>Enter your access code</Header>
              <Container as='p' textAlign='center'>
                Check your invitation for an access code. It should have six characters.
              </Container>
              <Container textAlign='center'>
                <Input
                  ref={this.captureNode}
                  placeholder='acd347'
                  value={accessCode}
                  onChange={onChange}
                  action>
                  <input
                    className='w8 menlo'
                    autoComplete='off'
                    autoCorrect='off'
                    autoCapitalize='off'
                    spellCheck='false'
                    onKeyPress={this.handleKeyPress}
                    disabled={checking} />
                  {/* above taken from https://stackoverflow.com/a/254716 */}
                  <Button
                    loading={checking}
                    onClick={onSubmit}
                    disabled={!showValidStatus || !isValid}>
                    go
                  </Button>
                </Input>
                {showValidStatus &&
                  <Message
                    color={isValid ? 'green' : 'red'}
                    icon={!isValid ? 'remove' : 'checkmark'}
                    header={`Access code is ${isValid ? 'valid' : 'invalid'}`}
                    list={errors.map(error => error.message)}
                    className='text-left' />
                }
                <Divider horizontal>OR</Divider>
                <Button onClick={continueOn}>
                  no access code
                </Button>
              </Container>
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

export default Gateway;
