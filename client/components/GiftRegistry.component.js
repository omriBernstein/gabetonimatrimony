import React, {Component} from 'react';
import {Container, Header} from 'semantic-ui-react';

import PitStopHeader from './PitStopHeader.component';
import BobbingDownArrow from './BobbingDownArrow.component';

const registryLinks = [{
  text: 'Crate & Barrel',
  url: 'https://m.crateandbarrel.com/gift-registry/gabe-venegas-and-toni-kane/r5721362'
}, {
  text: 'Amazon',
  url: 'https://www.amazon.com/wedding/gabe-venegas-toni-kane-tinahely-wicklow-may-2018/registry/1D18Y57VIQ5K3'
}];

const PayPalDonateButton = () => (
  <form
    action='https://www.paypal.com/cgi-bin/webscr'
    method='post'
    target='_blank'
    rel='noopener noreferrer'>
    <input
      type='hidden'
      name='cmd'
      value='_s-xclick' />
    <input
      type="hidden"
      name="encrypted"
      value='-----BEGIN PKCS7-----MIIHTwYJKoZIhvcNAQcEoIIHQDCCBzwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAuoJeaFInvoyaS+BYWV8vaKmazXd6H2rXm8uwv8EQzL3t5qkcM+VQ9VB79tcZUCiXkR+4fFg3u0BqAT/oW2HOaD6pUQpOu52W+XinV+BS+xBHhD0sDH54Qlb2FgFtzMORwWAgvOo3KaPojnzoyfA246rNlVxMquUtt2nDqDsFwOzELMAkGBSsOAwIaBQAwgcwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIYBLzlZr7x4+Agag3NluDse3zKxbTWPGoBo1iAxOqixQHFNV+/NqiyglgU7AuOKp20KLUC+luaL+zQC+Vr1pj8bWpgtUaGWB05lI/fF+xZ3k/DN8XobzWnLXxvZHjQW39i5+gQvfQQwGEwr2HUg6KoQJjYIXJ5w5FJdWSltA6qNzi0JLr4rUdSADBTIsKIzjutodGtAoVCBOZ4qRkDKaWquQ4VojEQj+xSRwNBrJ4fVb2tWqgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xODAxMjAxNjI0MjdaMCMGCSqGSIb3DQEJBDEWBBT0NNLJkZxSm8VejoLLOD7cM3+YLDANBgkqhkiG9w0BAQEFAASBgBzIP+3bjS+Bp0ePQZ3TwdR6r84hsq/1MOzuTFLU7+HnCNesZYO+RBYckXgx3ZPx80GRN2c05zjUULes+QwNxn4afREvZYNDTzwhS/L/doeqiTwH0k28vLcQcQqliwgcjKXivJ7ohHVMwRCZgqWdhzspLyQTk2gxuE5XOcHWfQPZ-----END PKCS7-----
    ' />
    <input
      type='image'
      src='https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif'
      border='0'
      name='submit'
      alt='PayPal - The safer, easier way to pay online!' />
    <img
      alt=''
      border='0'
      src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif'
      width='1'
      height='1' />
  </form>
);

class GiftRegistry extends Component {
  render () {
    return (
      <Container>
        <PitStopHeader content='Gift Registry' />
        <Container as='h2' textAlign='center'>
          <br />
          <Container as='p'>
            Welcome to our registry page! Thanks for scrolling down the country roads this far! If you would like to give us a wee gift, you have three ways to do so :)
          </Container>
          <Container as='p'>
            One of the most meaningful gifts you can give us is the gift of travel. We love to go on adventures to suppress our wanderlust and when we do, we stay at Airbnb!
          </Container>
          <Container as='p'>
            You can purchase us an Airbnb gift card <a target='_blank' rel='noopener noreferrer' href='https://www.amazon.com/gp/aw/d/B016PARWO0'>here</a>. Make sure to enter the following email into the "to" field of the form: gabetonimatrimony@gmail.com
          </Container>
          <Container as='p'>
            If that doesn’t tickle your fancy, you can donate to our wee travel fund, which we will use for flights! YEAH!
          </Container>
          <Container as='p'>
            <PayPalDonateButton />
          </Container>
          <Container as='p'>
            Alternatively (and equally appreciated), if a fork or a mug is more your thing...we have a few useful "bits and bobs" that we would love to have as we begin the greatest adventure of all as husband and wife. Check out the two registry links below.
          </Container>
          {registryLinks.map(({text, url}) => (
            <Header as='h1' textAlign='center' key={url}>
              <a
                href={url}
                target='_blank'
                rel='noopener noreferrer'>
                {text}
              </a>
            </Header>
          ))}
          <br />
          <Container as='p'>
            Thank you! Obrigado! Geruh-muh-ugg-it! ¡Gracias!
          </Container>
          <br />
          <BobbingDownArrow />
        </Container>
      </Container>
    );
  }
}

export default GiftRegistry;
