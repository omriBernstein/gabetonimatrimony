import React, {Component} from 'react';
import {Button} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import {scalePow} from 'd3-scale';
import throttle from 'lodash.throttle';

import withinBounds from '../utils/within-bounds';
import {pitStopUrls, excludesRegistry} from '../constants';

const steps = (isInvited) => [{
  content: 'Welcome'
}, {
  content: 'Our Story'
}, {
  content: 'The Proposal'
}, {
  content: 'Wedding Party'
}, {
  content: excludesRegistry ? 'Thanks' : 'Event Details'
}].concat(excludesRegistry ? [] : [{
  content: 'Attire'
}, {
  content: 'Gift Registry'
}, {
  content: isInvited ? 'RSVP' : 'Thanks'
}]).map((step, index) => Object.assign({}, step, {index}));

const baseFooterStyle = {
  position: 'fixed',
  bottom: 0,
  left: 0,
  right: 0,
  zIndex: 2,
  textAlign: 'center'
};

const dampingFactor = 2;

const scaleFirstHalf = scalePow()
.exponent(dampingFactor)
.domain([0, 0.5])
.range([0, 0.5])
.clamp(true);

const scaleSecondHalf = scalePow()
.exponent(-dampingFactor)
.domain([0.5, 1])
.range([0.5, 1])
.clamp(true);

const scaleFraction = function (fraction) {
  if (fraction < 0.5) {
    return scaleFirstHalf(fraction);
  } else {
    return scaleSecondHalf(fraction);
  }
};

class Navigate extends Component {
  constructor () {
    super();
    this.state = {
      widthDiff: null,
      leftOffset: 0
    };
    this.computedLeft = 0;
    this.handleResize = throttle(this.handleResize, 10);
  }
  componentDidMount () {
    setTimeout(this.handleResize, 50);
    window.addEventListener('resize', this.handleResize);
  }
  componentWillUnmount () {
    window.removeEventListener('resize', this.handleResize);
  }
  handleResize = () => {
    this.setState({
      widthDiff: window.innerWidth - this._node.scrollWidth,
      leftOffset: 0
    });
  }
  captureNode = (node) => {
    this._node = node;
  }
  computeLeft () {
    const {fractionComplete} = this.props;
    const {widthDiff} = this.state;
    if (widthDiff < 0 && fractionComplete) {
      const scaled = scaleFraction(fractionComplete);
      return scaled * widthDiff;
    } else {
      return 0;
    }
  }
  setComputedLeft () {
    const newLeft = this.computeLeft();
    if (newLeft === this.computedLeft) return;
    if (this.state.leftOffset !== 0) {
      setTimeout(() => {
        this.setState({leftOffset: 0});
      });
    }
    this.computedLeft = newLeft;
  }
  computeFooterStyle () {
    this.setComputedLeft();
    return Object.assign({}, baseFooterStyle, {
      left: this.computedLeft + this.state.leftOffset
    });
  }
  onTouchStart = ({touches}) => {
    this._touchBase = touches[0].pageX;
  }
  onTouchMove = (event) => {
    const touchDelta = (event.touches[0].pageX - this._touchBase) / 30;
    event.stopPropagation();
    event.preventDefault();
    this.onWheelOrTouch(touchDelta);
  }
  onWheel = (event) => {
    event.stopPropagation();
    event.preventDefault();
    this.onWheelOrTouch(event.deltaX);
  }
  onWheelOrTouch (deltaX) {
    if (this.state.widthDiff >= 0) return;
    const newLeftOffset = withinBounds(
      this.state.leftOffset + deltaX,
      this.state.widthDiff - this.computedLeft,
      -this.computedLeft
    );
    this.setState({
      leftOffset: newLeftOffset
    });
  }
  pitStopIsLoading (stop) {
    const {currentStop, location: {pathname: currentUrl}, isInvited} = this.props;
    return pitStopUrls(isInvited)[stop] === currentUrl && stop !== currentStop;
  }
  render () {
    const {currentStop, isInvited} = this.props;
    const footerStyle = this.computeFooterStyle();
    return (
      <div
        style={footerStyle}
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onWheel={this.onWheel}>
        <div ref={this.captureNode} style={{display: 'inline-block'}}>
          <Button.Group>
            {steps(isInvited).map(({content, index}) => (
              <Link
                key={index}
                to={pitStopUrls(isInvited)[index]}
                style={{
                  flex: '1 0 auto'
                }}>
                <Button
                  size='big'
                  active={currentStop === index}
                  color='black'
                  loading={this.pitStopIsLoading(index)}
                  className={currentStop !== index ? 'hover-lighten' : 'hover-no-pointer'}
                  style={{
                    borderTop: `1px solid ${currentStop === index ? 'white' : '#556B2F'}`,
                    borderRadius: 0,
                    backgroundColor: currentStop === index ? '#475927' : '#556B2F'
                  }}>
                  {content}
                </Button>
              </Link>
            ))}
          </Button.Group>
        </div>
      </div>
    );
  }
}

export default Navigate;
