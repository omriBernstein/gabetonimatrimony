import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import Navigate from '../components/Navigate.component';
import {pitStops} from '../constants';

const stopTimes = Array.from(pitStops(true).keys());

const rsvpStopTime = stopTimes[stopTimes.length - 2];

const NavigateContainer = withRouter(connect(
  ({road: {currentStop, currentTime}, me}) => ({
    currentStop,
    fractionComplete: Math.min(currentTime / rsvpStopTime, 1),
    isInvited: me && me.hasOwnProperty('id')
  })
)(Navigate));

export default NavigateContainer;
