import React, {Component} from 'react';
import {connect} from 'react-redux';

import Gateway from '../components/Gateway.component';
import {login, logout} from '../ducks/me.duck';
import {passGateway} from '../ducks/passed-gateway.duck';

class GatewayLocalStateContainer extends Component {
  constructor () {
    super();
    this.state = {
      accessCode: '',
      checking: false,
      errors: [],
      showValidStatus: false
    };
  }
  handleChange = (event) => {
    const accessCode = event.target.value.trim();
    if (accessCode.length >= 6) {
      this.handleCheck(accessCode);
    } else {
      this.setState({
        accessCode,
        errors: [],
        showValidStatus: false
      });
    }
  }
  handleCheck (accessCode) {
    this.setState({
      checking: true,
      accessCode
    });
    const {login, logout} = this.props;
    login(accessCode)
    .then(
      () => {
        this.setState({errors: []});
        return logout();
      },
      (error) => {
        const errors = [];
        if (error.response.status === 401) {
          errors.push(new Error('No such access code, please check your invitation again'));
        }
        this.setState({
          errors
        });
      }
    )
    .then(() => {
      this.setState({
        checking: false,
        showValidStatus: true
      });
    });
  }
  handleSubmit = () => {
    const {passGateway, login} = this.props;
    passGateway();
    login(this.state.accessCode);
  }
  render () {
    const {
      accessCode,
      errors,
      checking,
      showValidStatus
    } = this.state;
    const {passGateway} = this.props;
    return (
      <Gateway
        accessCode={accessCode}
        onChange={this.handleChange}
        errors={errors}
        showValidStatus={showValidStatus}
        onSubmit={this.handleSubmit}
        checking={checking}
        continueOn={passGateway} />
    );
  }
}

const GatewayContainer = connect(
  null,
  {login, logout, passGateway}
)(GatewayLocalStateContainer);

export default GatewayContainer;
