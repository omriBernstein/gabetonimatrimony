import {connect} from 'react-redux';

import ProposalVideo from '../components/ProposalVideo.component';
import {play, pause} from '../ducks/background-audio.duck';

const ProposalVideoContainer = connect(
  ({windowHasFocus}) => ({windowHasFocus}),
  {playBackgroundAudio: play, pauseBackgroundAudio: pause}
)(ProposalVideo);

export default ProposalVideoContainer;
