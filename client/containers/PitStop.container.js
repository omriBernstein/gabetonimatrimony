import {connect} from 'react-redux';

import PitStop from '../components/PitStop.component';
import {setForwards, setBackwards} from '../ducks/delta-active.duck';

const PitStopContainer = connect(
  ({road: {currentStop, loading}, me}) => ({
    currentStop,
    loading,
    isInvited: me && me.hasOwnProperty('id')
  }),
  {setForwards, setBackwards}
)(PitStop);

export default PitStopContainer;
