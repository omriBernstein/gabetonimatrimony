import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {setStop} from '../ducks/road.duck';
import RouteToPitStop from '../components/RouteToPitStop.component';

const RouteToPitStopContainer = withRouter(connect(
  ({road: {currentStop, loading}, me}) => ({
    currentStop,
    isInvited: me && me.hasOwnProperty('id'),
    loading
  }),
  {setStop}
)(RouteToPitStop));

export default RouteToPitStopContainer;
