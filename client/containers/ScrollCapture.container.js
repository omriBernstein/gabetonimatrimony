import {connect} from 'react-redux';

import ScrollCapture from '../components/ScrollCapture.component';
import {applyDelta, unstick} from '../ducks/road.duck';
import {setForwards, setBackwards} from '../ducks/delta-active.duck';

const ScrollCaptureContainer = connect(
  ({road: {currentStop, stuck}, deltaActive: {forwards, backwards}}) => ({
    stuck,
    deltaActive: {
      forwards: forwards || currentStop === null,
      backwards: backwards || currentStop === null
    }
  }),
  {applyDelta, unstick, setForwards, setBackwards}
)(ScrollCapture);

export default ScrollCaptureContainer;
