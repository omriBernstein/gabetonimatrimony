import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import Entry from '../components/Entry.component';
import {fetchMe} from '../ducks/me.duck';
import {passGateway} from '../ducks/passed-gateway.duck';

const EntryContainer = connect(
  ({passedGateway}) => ({passedGateway}),
  {loadMe: fetchMe, passGateway}
)(Entry);

export default withRouter(EntryContainer);
