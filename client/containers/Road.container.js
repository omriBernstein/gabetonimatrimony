import {connect} from 'react-redux';

import Road from '../components/Road.component';
import {setDuration, setCurrentTime, roadLoaded} from '../ducks/road.duck';

const RoadContainer = connect(
  ({road: {prospectiveTime, currentStop, loading}}) => ({
    prospectiveTime,
    blurred: currentStop !== null,
    loading
  }),
  {setDuration, setCurrentTime, roadLoaded}
)(Road);

export default RoadContainer;
