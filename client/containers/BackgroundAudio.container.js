import {connect} from 'react-redux';

import BackgroundAudio from '../components/BackgroundAudio.component';

const BackgroundAudioContainer = connect(
  ({backgroundAudio: {playing}, windowHasFocus}) => ({
    playing,
    windowHasFocus
  })
)(BackgroundAudio);

export default BackgroundAudioContainer;
