import {connect} from 'react-redux';

import RSVPForm from '../components/RSVPForm.component';
import {updateMe} from '../ducks/me.duck';

const RSVPFormContainer = connect(
  ({me}) => ({me}),
  {updateMe}
)(RSVPForm);

export default RSVPFormContainer;
