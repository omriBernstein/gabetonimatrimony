import store from './store';

export const roadVideoDurationIsSet = new Promise((resolve) => {
  const unsubscribe = store.subscribe(() => {
    const {road: {duration}} = store.getState();
    if (duration > 0) {
      resolve(duration);
      unsubscribe();
    }
  });
});
