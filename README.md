# GabeToniMatrimony

The source code for Gabe and Toni's wedding website. See it live here (best viewed in desktop Safari or Edge): https://gabetonimatrimony.com

## Running locally

You'll need to have [node](https://nodejs.org/en/download/package-manager/), [postgres](https://www.postgresql.org/download/), and [yarn](https://yarnpkg.com/lang/en/docs/install/#mac-stable) installed.

- [Create a local postgres database](https://www.postgresql.org/docs/current/tutorial-createdb.html) called `gony_wedding`
- Run `yarn install` (from this project folder's root)
- If you want (not terribly important), run `npm run dev-random-seed` to seed the database (which will print all the access codes if you want to use one)

Now you should be able to `npm run dev-start` and open `http://localhost:1337` to start and view the application.
