import consume from '../utils/consume';
import JsonEndpoint from '../utils/JsonEndpoint';
import {meProvider} from '../request-data-providers';
import HttpError from '../utils/HttpError';

export const showProvider = consume({me: meProvider},
  ({me}) => me
);
export const show = JsonEndpoint(showProvider);

export const loginProvider = consume({me: meProvider},
  ({me}) => {
    if (me) return me;
    throw HttpError(401);
  }
);
export const login = JsonEndpoint(loginProvider);

export const logoutProvider = () => null;
export const logout = JsonEndpoint(logoutProvider)
.successStatus(204);
