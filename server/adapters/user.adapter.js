import consume from '../utils/consume';
import JsonEndpoint from '../utils/JsonEndpoint';
import {bodyProvider, userProvider} from '../request-data-providers';

export const updateProvider = consume({body: bodyProvider, user: userProvider},
  ({body, user}) => user.update(body)
);
export const update = JsonEndpoint(updateProvider);

