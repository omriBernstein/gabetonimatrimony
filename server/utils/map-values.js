const mapValues = (obj, mapper) => {
  const mapped = {};
  for (const k of Object.keys(obj)) {
    mapped[k] = mapper(obj[k]);
  }
  return mapped;
};

export default mapValues;
