const firstCalls = new WeakMap();

const once = fn => function (...args) {
  if (firstCalls.has(fn)) return firstCalls.get(fn);
  return firstCalls.set(fn, fn.apply(this, args));
};

export default once;
