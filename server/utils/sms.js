import Twilio from 'twilio';
import Bluebird from 'bluebird';

const accountSid = 'AC4047272866fa5bd5ff337a5a947b5e81';
const authToken = process.env.TWILIO_TOKEN;

export const twilio = !authToken ? {} : new Twilio(accountSid, authToken);

export const sendToGabeAndToni = !authToken ? function () {
  console.error('NOT SENDING SMS MESSAGE BECAUSE THERE IS NO TWILIO AUTH TOKEN');
} : function (body) {
  return Bluebird.join(
    twilio.messages.create({
      body,
      to: '+17816326488',
      from: '+17814173275'
    }),
    twilio.messages.create({
      body,
      to: '+17375294757',
      from: '+17814173275'
    })
  );
};
