import Bluebird from 'bluebird';

import mapValues from './map-values';

const consume = (providers, consumer) => function (...args) {
  const tryProvider = provider => Bluebird.try(() => provider.apply(this, args));
  return Bluebird.props(mapValues(providers, tryProvider))
  .then(provided => consumer(provided, ...args));
};

export default consume;
