import http from 'http';

class HttpErrorClass extends Error {
  constructor (status, message = HttpErrorClass.statusToMessage(status)) {
    super();
    const error = new Error(message);
    Object.setPrototypeOf(error, HttpErrorClass.prototype);
    error.name = HttpErrorClass.name;
    error.status = status;
    return error;
  }
  static statusToMessage (status) {
    return http.STATUS_CODES[status];
  }
  static middleware (status, message) {
    return (request, response, next) => next(HttpError(status, message));
  }
}

function HttpError (...args) {
  return new HttpErrorClass(...args);
}
HttpError.statusToMessage = HttpErrorClass.statusToMessage;
HttpError.middleware = HttpErrorClass.middleware;

export default HttpError;
