import shuffle from './shuffle';

const chars = new Set('acdefhijkmnpqrtuvwxy347');

const generateAccessCode = function (length) {
  if (length > chars.size) {
    return generateAccessCode(chars.size) + generateAccessCode(chars.size - length);
  }
  return shuffle(Array.from(chars)).slice(0, length).join('');
};

export default generateAccessCode;
