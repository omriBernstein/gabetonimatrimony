import Bluebird from 'bluebird';

function JsonEndpoint (executor) {
  const endpoint = (request, response, next) => {
    Bluebird.try(() => executor(request))
    .then(data => {
      response.status(endpoint._successStatus).json(data);
    })
    .catch(next);
  };
  Object.setPrototypeOf(endpoint, JsonEndpoint.prototype);
  endpoint._successStatus = 200;
  return endpoint;
}

JsonEndpoint.prototype.successStatus = function (status) {
  this._successStatus = status;
  return this;
};

export default JsonEndpoint;
