const shuffle = function (arr) {
  let right = arr.length;
  while (right) {
    const idx = Math.floor(Math.random() * right--);
    [arr[right], arr[idx]] = [arr[idx], arr[right]];
  }
  return arr;
};

export default shuffle;
