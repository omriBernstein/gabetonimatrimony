import express from 'express';
import path from 'path';
import volleyball from 'volleyball';

import apiRouter from './api.router';
import HttpError from '../utils/HttpError';
import constants from '../constants';

const rootRouter = express.Router();

const notFound = HttpError.middleware(404);

// logging middleware
rootRouter.use(volleyball);

// static file serving middleware
const publicPath = path.resolve(constants.projectRoot, 'public');
rootRouter.use('/public',
  express.static(publicPath),
  notFound
);

// api middleware
rootRouter.use('/api',
  apiRouter,
  notFound
);

// server index.html by default for non-api-or-public GET requests
const indexHtmlPath = path.resolve(publicPath, 'index.html');
rootRouter.get('/**', (req, res) => res.sendFile(indexHtmlPath));

export default rootRouter;
