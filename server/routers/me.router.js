import express from 'express';

import {
  show,
  login,
  logout
} from '../adapters/me.adapter';

const meRouter = express.Router()
.get('/', show)
.put('/', login)
.delete('/', logout);

export default meRouter;
