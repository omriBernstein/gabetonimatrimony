import express from 'express';

import {update} from '../adapters/user.adapter';

const userRouter = express.Router()
.put('/:id', update);

export default userRouter;
