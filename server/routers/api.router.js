import express from 'express';

import meRouter from './me.router';
import userRouter from './user.router';

const apiRouter = express.Router()
.use('/me', meRouter)
.use('/users', userRouter);

export default apiRouter;
