import {ARRAY, STRING, TEXT, INTEGER, BOOLEAN} from 'sequelize';

import db from './db-connection';
import generateAccessCode from '../utils/generate-access-code';
import {sendToGabeAndToni} from '../utils/sms';

const instanceMethods = {};

const classMethods = {
  authenticate (accessCode) {
    return this.findOne({where: {accessCode: `${accessCode}`}})
    .then((user) => {
      if (user) return user;
      throw new Error('Incorrect access code');
    });
  }
};

const attendingMessage = {
  true: 'indeed',
  false: 'not',
  default: 'not sure whether they are'
};
function generateAttendanceMessage ({guestNames, email, attending, rsvpMessage}) {
  return (
    (guestNames.length > 0 ? guestNames.join(', ') : email) +
    (guestNames.length > 1 ? 'has' : 'have') +
    ' submitted that they are ' +
    attendingMessage[attending] +
    ' attending. ' +
    (!rsvpMessage ? 'They sent no message.' : 'They sent the message: ' + rsvpMessage)
  );
}

const User = db.define('user', {
  categories: ARRAY(STRING),
  accessCode: {
    type: STRING,
    allowNull: false,
    defaultValue: function () {
      return generateAccessCode(6);
    }
  },
  email: {
    type: STRING,
    validate: {
      isEmail: true
    }
  },
  title: STRING,
  address: TEXT,
  foodRequirements: TEXT,
  guestNames: ARRAY(TEXT),
  rsvpMessage: TEXT,
  allowedGuests: INTEGER,
  attending: BOOLEAN
}, {
  instanceMethods,
  classMethods,
  hooks: {
    afterUpdate: function (user) {
      sendToGabeAndToni(generateAttendanceMessage(user));
    }
  }
});

export default User;
