import Bluebird from 'bluebird';

import db, {User} from '.';

function doTimes (n, fn) {
  const arr = [];
  while (n-- > 0) {
    arr.push(fn());
  }
  return arr;
}

function generateUsers () {
  return Bluebird.all(
    doTimes(150, () => {
      return User.create({})
      .tap(user => {
        console.log(user.accessCode);
      });
    })
  );
}

function seed () {
  return db.sync({force: true})
  .then(generateUsers)
  .then(
    () => console.log('Seed successful'),
    (error) => console.error('Seed failed', error),
  )
  .then(() => {
    process.exit();
  });
}

seed();
