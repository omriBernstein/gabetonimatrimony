import db from './db-connection';

import _User from './user.model';

export const User = _User;

const allModels = {
  User
};

const defineAssociations = model => {
  model.defineAssociations && model.defineAssociations(allModels);
};

Object.values(allModels).forEach(defineAssociations);

export default db;
