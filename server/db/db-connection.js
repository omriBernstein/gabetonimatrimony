import Sequelize from 'sequelize';

import constants from '../constants';

const db = new Sequelize(constants.database.URI, {
  logging: false,
  define: {
    underscored: true
  }
});

export default db;
