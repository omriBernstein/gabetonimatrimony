const idProvider = request => Number(request.params.id);

export default idProvider;
