import Bluebird from 'bluebird';

const bodyProvider = request => {
  return new Promise((resolve, reject) => {
    let bodyStr;
    request.on('data', chunk => {
      if (chunk) bodyStr += chunk;
    });
    request.on('end', () => {
      if (bodyStr.startsWith('undefined')) {
        bodyStr = bodyStr.slice(9);
      }
      Bluebird.try(() => JSON.parse(bodyStr))
      .then(resolve, reject);
    });
  });
};

export default bodyProvider;
