import consume from '../utils/consume';
import {User} from '../db';
import HttpError from '../utils/HttpError';

import idProvider from './id.provider';
import meProvider from './me.provider';

const userProvider = consume({userId: idProvider, me: meProvider},
  ({userId, me}) => {
    if (me.id !== userId) throw HttpError(403);
    return User.findById(userId)
    .tap(user => {
      if (!user) throw HttpError(404);
    });
  }
);

export default userProvider;
