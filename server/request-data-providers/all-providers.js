import _queryProvider from './query.provider';
import _idProvider from './id.provider';
import _bodyProvider from './body.provider';
import _meProvider from './me.provider';
import _userProvider from './user.provider';

export const queryProvider = _queryProvider;
export const idProvider = _idProvider;
export const bodyProvider = _bodyProvider;
export const meProvider = _meProvider;
export const userProvider = _userProvider;
