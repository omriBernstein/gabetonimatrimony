import consume from '../utils/consume';
import {User} from '../db';

import queryProvider from './query.provider';

const meProvider = consume({query: queryProvider},
  ({query}) => {
    if (!query.accessCode) return null;
    return User.authenticate(query.accessCode).catch(() => null);
  }
);

export default meProvider;
