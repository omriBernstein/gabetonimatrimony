const queryProvider = request => request.query;

export default queryProvider;
