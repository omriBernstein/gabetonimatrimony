import db from  './db';
import app from  './express-app';
import constants from './constants';

const {port, database: {forceSync: force}} = constants;

const syncDb = () => db.sync({force});

const startListening = () => new Promise((resolve, reject) => {
  app.listen(port, err => (err ? reject(err) : resolve()));
});

const startPromise = syncDb().then(startListening)
.then(
  () => {
    console.log('Server started:', `http://localhost:${port}`);
  },
  err => {
    console.error('Server failed to start');
    console.error(err);
  }
);

export default startPromise;
