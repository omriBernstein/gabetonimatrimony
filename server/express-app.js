import express from 'express';
import rootRouter from './routers';

const app = express();

app.use(rootRouter);

export default app;
