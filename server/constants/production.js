const productionConstants = {
  database: {
    URI: process.env.DATABASE_URL,
    forceSync: false
  },
  port: process.env.PORT
};

module.exports = productionConstants;
