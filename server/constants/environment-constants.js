import path from 'path';

const shared = {
  projectRoot: path.resolve(__dirname, '..', '..')
};

let loadedConstants;

if (process.env.NODE_ENV === 'production') {
  loadedConstants = require('./production');
} else {
  loadedConstants = require('./default');
}

const allConstants = {
  ...process.env,
  ...shared,
  ...loadedConstants
};

export default allConstants;
