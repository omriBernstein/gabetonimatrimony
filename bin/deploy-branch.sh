#!/usr/bin/env bash

# execution deps...
# - git
# - npm
# - local npm script "production-build", that puts build artifacts in the "public" folder and/or "server-build" folder
# - local git remote called "heroku" pointing to deploy target

current_branch=$(git rev-parse --abbrev-ref HEAD)
source_branch='master'
target_branch='auto-branch-heroku-deploy'

if [[ $(git status --porcelain 2> /dev/null | grep -v '$\?\?' | tail -n1) != "" ]]; then
  git stash && stashed='true'
fi

git checkout $source_branch &&
git checkout -b $target_branch &&
npm run production-build &&
git add -f public/* server-build/ &&
git commit -m "chore: build for deploy (automated commit)" &&
git push -f heroku $target_branch:master

git checkout $current_branch

if git rev-parse --quiet --verify $target_branch; then
  git branch -D $target_branch
  rm -rf server-build/
fi

if [[ $stashed == 'true' ]]; then
  git stash pop
fi
