#!/usr/bin/env bash

# execution deps...
# - ffmpeg
# - mapfile (bash v4)

inpath="$1"
outdir="$2"
fps="${3:-8}"
size="${4:-1080x720}"

mapfile -t times < /dev/stdin

generateone () {
  start="$1"
  end="$2"
  outname="$3"
  outpath="$outdir/$outname.gif"
  # ffmpeg command adapted from https://superuser.com/a/436109 and https://trac.ffmpeg.org/wiki/Seeking#Notes
  ffmpeg -ss "$start" -i "$inpath" -pix_fmt rgb8 -r "$fps" -s "$size" -to "$end" -copyts "$outpath"
}

mkdir -p "$outdir"
for (( index = 0 ; index < ${#times[@]} - 1 ; index++ )); do
  generateone ${times[index]} ${times[index+1]} $index
done
